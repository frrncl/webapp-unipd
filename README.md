# Web Applications (webapp) - Code Examples

This repository contains the source code of the examples complementing the lectures of the *Web Applications* course.

*Web Applications* is a course of the 

* [Master Degree in Computer Engineering](https://lauree.dei.unipd.it/lauree-magistrali/computer-engineering/)
* [Master Degree in ICT for Internet and Multimedia](https://lauree.dei.unipd.it/lauree-magistrali/ict-for-internet-multimedia-mime/)
* [Master Degree in Cybersecurity](https://cybersecurity.math.unipd.it/)

of the  [Department of Information Engineering](https://www.dei.unipd.it/en/), [University of Padua](https://www.unipd.it/en/), Italy. *Web Applications* is part of the teaching activities of the [Intelligent Interactive Information Access (IIIA) Hub](http://iiia.dei.unipd.it/).

Copyright and license information can be found in the file LICENSE.  Additional information can be found in the file NOTICE.