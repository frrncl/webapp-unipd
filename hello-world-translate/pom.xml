<?xml version="1.0"?>
<!--
 
 Copyright 2018 University of Padua, Italy
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 
 Author: Nicola Ferro (ferro@dei.unipd.it)
 Version: 1.0
 Since: 1.0
-->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>it.unipd.dei.webapp</groupId>

	<artifactId>hello-world-translate</artifactId>
	
	<version>1.00</version>

	<packaging>jar</packaging>

	<!-- Project description elements -->
	<name>Translate Hello World</name>

	<description>Translate "Hello, world!" to the requested language and
				 writes the translation on the console</description>

	<url>https://bitbucket.org/frrncl/webapp-unipd</url>

	<inceptionYear>2018</inceptionYear>

	<developers>
		<developer>
			<id>nf</id>
			<name>Nicola Ferro</name>
			<email>ferro@dei.unipd.it</email>
			<url>http://www.dei.unipd.it/~ferro/</url>
		</developer>
	</developers>

	<licenses>
		<license>
			<name>The Apache Software License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<organization>
		<name>Department of Information Engineering (DEI), University of Padua, Italy</name>
		<url>http://www.dei.unipd.it/en/</url>
	</organization>

	<!-- Build settings -->
	
	<!-- Specifies the encoding to be used for project source files -->
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>
	
	<!-- Configuration of the default build lifecycle -->
	<build>
		<defaultGoal>compile</defaultGoal>
		
		<!-- source code folder -->
		<sourceDirectory>${basedir}/src/main/java</sourceDirectory>
		
		<!-- compiled code folder -->
		<directory>${basedir}/target</directory>
		
		<!-- name of the generated package -->
		<finalName>${project.artifactId}-${project.version}</finalName>

		<!-- configuration of the plugins for the different goals -->
		<plugins>
		
			<!-- compiler plugin: source and target code is for Java 1.8 -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.7.0</version>
				<configuration>
					<source>17</source>
					<target>17</target>
				</configuration>
			</plugin>
			
			
			<!-- javadoc plugin: output in the javadoc folder -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>3.0.0</version>
				<configuration>
					<reportOutputDirectory>${basedir}/javadoc</reportOutputDirectory>
					<author>true</author>
					<nosince>false</nosince>
					<show>protected</show>
				</configuration>
			</plugin>
			
			<!-- generates jar files including any dependencies -->
			<plugin>
				<artifactId>maven-assembly-plugin</artifactId>
				<version>3.1.0</version>
				<configuration>
					<descriptorRefs>
						<descriptorRef>jar-with-dependencies</descriptorRef>
					</descriptorRefs>
				</configuration>
				<executions>
					<execution>
						<id>make-assembly</id> <!-- this is used for inheritance merges -->
						<phase>package</phase> <!-- bind to the packaging phase -->
						<goals>
							<goal>single</goal> <!-- the only goal of the assembly plugin -->
						</goals>
					</execution>
				</executions>
			</plugin>
			
		</plugins>
	</build>
	
	<!-- Dependencies -->
	<dependencies>
		<dependency>
			<groupId>com.googlecode.json-simple</groupId>
			<artifactId>json-simple</artifactId>
			<version>1.1.1</version>
		</dependency>
	</dependencies>
</project>