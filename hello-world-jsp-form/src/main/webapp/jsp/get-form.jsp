<!--
Copyright 2019 University of Padua, Italy

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Author: Nicola Ferro (ferro@dei.unipd.it)
Version: 1.0
Since: 1.0
-->

<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="en">
<head>

    <c:import url="/jsp/include/head.jsp"/>

    <title>Basic Web Application with JavaServer Pages - GET Form Example</title>

</head>

<body>

<div class="container">

    <!-- header -->
    <header class="mt-5 mb-5">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-4">Basic Web Application with JavaServer Pages</h1>
                <p class="lead">GET Form Example</p>
            </div>
        </div>
    </header>

    <!-- body -->
    <div class="content mt-5 mb-5">

        <div class="row justify-content-center">

            <form method="get" action="<c:url value="/jsp/hello-world-param.jsp"/>"
                  class="shadow-sm p-3 bg-light rounded">

                <div class="form-group row align-items-center mt-5">
                    <div class="col-md-2 text-right">
                        <label class="col-form-label" for="helloName">Name</label>
                    </div>
                    <div class="col-md-10 text-center">
                        <div class="input-group mb3">
                            <input class="form-control form-control-lg" type="text" id="helloName" name="helloName"
                                   size="45" required placeholder="Please enter your name">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="fas fa-id-badge fa-2x"></i></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row mt-4">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-outline-dark btn-lg" type="submit">
                            Submit <i class="fas fa-signature fa-2x align-middle ml-3"></i>
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div> <!-- End of Content-->

    <!-- footer -->
    <c:import url="/jsp/include/footer.jsp"/>

</div> <!-- /.container -->

<c:import url="/jsp/include/foot.jsp"/>
</body>
</html>
