<!--
Copyright 2019 University of Padua, Italy

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Author: Nicola Ferro (ferro@dei.unipd.it)
Version: 1.0
Since: 1.0
-->
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">
<head>

    <c:import url="/jsp/include/head.jsp"/>

    <title>Basic Web Application with JavaServer Pages - JSP using request parameter</title>

</head>

<div class="container">

    <!-- header -->
    <header class="mt-5 mb-5">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-4">Basic Web Application with JavaServer Pages</h1>
                <p class="lead"><code class="text-secondary">hello-world-param.jsp</code> Response</p>
            </div>
        </div>
    </header>

    <!-- body -->
    <div class="content mt-5 mb-5">
        <div class="row justify-content-center">

            <!-- even if the field is required in the form, this page may be called directly.
              Therefore, you need to  validate form fields again -->
            <c:choose>
                <c:when test="${empty param.helloName}">
                    <div class="col-md-6 h2 text-left alert alert-danger" role="alert">
                        Please, enter your name!
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="col-md-6 h2 text-left text-light bg-dark">
                        Hello, <c:out value="${param.helloName}"/>!
                    </div>
                </c:otherwise>
            </c:choose>

        </div>

        <div class="row justify-content-center">
            <div class="col-md-6 h5 text-left text-muted">

                <!-- use a java.util.Date object to hold the current date -->
                <jsp:useBean id="now" class="java.util.Date"/>

                <!-- set the locale to British English -->
                <fmt:setLocale value="en_UK"/>

                <!-- format the date and time according to that locale -->
                on <fmt:formatDate value="${now}" type="date" dateStyle="long"/>
                at <fmt:formatDate value="${now}" type="time" timeStyle="long"/>
            </div>
        </div>
    </div><!-- End of Content-->

    <!-- footer -->
    <c:import url="/jsp/include/footer.jsp"/>

</div> <!-- /.container -->

<c:import url="/jsp/include/foot.jsp"/>
</body>
</html>
