<!--
Copyright 2019 University of Padua, Italy

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Author: Nicola Ferro (ferro@dei.unipd.it)
Version: 1.0
Since: 1.0
-->
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>

    <c:import url="/jsp/include/head.jsp"/>

    <title>Basic Web Application with JavaServer Pages</title>

</head>

<body>

<div class="container">

    <!-- header -->
    <header class="mt-5 mb-5">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-4">Basic Web Application with JavaServer Pages</h1>
            </div>
        </div>
    </header>

    <!-- body -->
    <div class="content mt-5 mb-5">

        <div class="card-deck">

            <!-- Start Hello World card-->
            <div class="card">
                <img src="<c:url value="/media/hello.png"/>" class="card-img-top" alt="Hello World card image">
                <div class="card-body">
                    <h5 class="card-title">Minimal JSP page</h5>
                    <p class="card-text text-center">
                        <code class="badge-pill badge-primary p-2">hello-world.jsp</code>
                        <a href="<c:url value="/jsp/hello-world.jsp"/>" target="_blank"
                           title="Click to run the example"><i class="fas fa-cogs fa-3x align-middle ml-3"></i></a>
                    </p>
                    <p class="card-text">
                        <small class="text-muted">It just says "Hello, world!"</small>
                    </p>
                </div>
            </div>
            <!-- End Hello World card-->

            <!-- Start GET form card-->
            <div class="card">
                <img src="<c:url value="/media/get.png"/>" class="card-img-top" alt="Get form card image">
                <div class="card-body">
                    <h5 class="card-title">Minimal GET form JSP page</h5>
                    <p class="card-text text-center">
                        <code class="badge-pill badge-primary p-2">get-form.jsp</code>
                        <a href="<c:url value="/jsp/get-form.jsp"/>" target="_blank"
                           title="Click to run the example"><i class="fas fa-cogs fa-3x align-middle ml-3"></i></a>
                    </p>
                    <p class="card-text">
                        <small class="text-muted">It asks for your name using a GET form and replies "Hello, &lt;your-name&gt;!"
                            with the current time and date.
                        </small>
                    </p>
                </div>
            </div>
            <!-- End GET form card-->

            <!-- Start Post form card-->
            <div class="card">
                <img src="<c:url value="/media/post.png"/>" class="card-img-top" alt="Post form card image">
                <div class="card-body">
                    <h5 class="card-title">Minimal POST form JSP page</h5>
                    <p class="card-text text-center">
                        <code class="badge-pill badge-primary p-2">post-form.jsp</code>
                        <a href="<c:url value="/jsp/post-form.jsp"/>" target="_blank"
                           title="Click to run the example"><i class="fas fa-cogs fa-3x align-middle ml-3"></i></a>
                    </p>
                    <p class="card-text">
                        <small class="text-muted">It asks for your name using a POST form and replies "Hello, &lt;your-name&gt;!"
                            with the current time and date.
                        </small>
                    </p>
                </div>
            </div>
            <!-- End POST form card-->

        </div> <!-- End Card Deck-->

    </div> <!-- End of Content-->

    <!-- footer -->
    <c:import url="/jsp/include/footer.jsp"/>

</div> <!-- /.container -->

<c:import url="/jsp/include/foot.jsp"/>
</body>
</html>
