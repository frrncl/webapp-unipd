<!--
Copyright 2019 University of Padua, Italy

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Author: Nicola Ferro (ferro@dei.unipd.it)
Version: 1.0
Since: 1.0
-->

<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>

    <c:import url="/jsp/include/head.jsp"/>

    <title>Basic Web Application with JavaServer Pages - Hello, world</title>

</head>

<body>

<div class="container">

    <!-- header -->
    <header class="mt-5 mb-5">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-4">Basic Web Application with JavaServer Pages</h1>
                <p class="lead"><code class="text-secondary">hello-world.jsp</code> Response</p>
            </div>
        </div>
    </header>

    <!-- body -->
    <div class="content mt-5 mb-5">
        <div class="row justify-content-center">
            <div class="col-md-6 h2 text-left text-light bg-dark">
            Hello, world!
            </div>
        </div>
    </div><!-- End of Content-->

    <!-- footer -->
    <c:import url="/jsp/include/footer.jsp"/>

</div> <!-- /.container -->

<c:import url="/jsp/include/foot.jsp"/>
</body>
</html>
