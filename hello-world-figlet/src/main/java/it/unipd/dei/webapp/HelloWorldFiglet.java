/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unipd.dei.webapp;

import com.github.dtmo.jfiglet.FigFontResources;
import com.github.dtmo.jfiglet.FigletRenderer;

import java.io.IOException;

/**
 * Sample class to say "Hello, world", using JFiglet.
 *
 * See <a href="https://github.com/dtmo/jfiglet" target="_blank">https://github.com/dtmo/jfiglet</a>.
 * 
 * @author Nicola Ferro  (ferro@dei.unipd.it)
 * @version 1.0
 * @since 1.0
 */
public class HelloWorldFiglet {

	/**
	 * Main method of the class.
	 * 
	 * Prints "Hello, world!", using JFiglet.
	 *
	 * You can pick one of the following Figlet fonts:
	 * <ul>
	 *     <li>Banner</li>
	 *     <li>Big</li>
	 *     <li>Block</li>
	 *     <li>Bubble</li>
	 *     <li>Digital</li>
	 *     <li>Ivrit</li>
	 *     <li>Lean</li>
	 *     <li>Mini</li>
	 *     <li>Mnemonic</li>
	 *     <li>Script</li>
	 *     <li>Shadow</li>
	 *     <li>Slant</li>
	 *     <li>Small</li>
	 *     <li>SmScript</li>
	 *     <li>SmShadow</li>
	 *     <li>SmSlant</li>
	 *     <li>Standard</li>
	 *     <li>Terminal</li>
	 * </ul>
	 *
	 * @param args command line arguments.
	 * 				{@code args[0]} is one of the Figlet fonts above.
	 *
	 * @throws IOException if unable to load the required font.
	 */
	public static void main(String[] args) throws IOException {

		// if there are no argument, print a small help
		if (args.length == 0) {
			System.out.printf("Please, pick on of the following Figlet fonts:%n");
			System.out.printf("- Banner%n");
			System.out.printf("- Big%n");
			System.out.printf("- Block%n");
			System.out.printf("- Bubble%n");
			System.out.printf("- Digital%n");
			System.out.printf("- Ivrit%n");
			System.out.printf("- Lean%n");
			System.out.printf("- Mini%n");
			System.out.printf("- Mnemonic%n");
			System.out.printf("- Script%n");
			System.out.printf("- Shadow%n");
			System.out.printf("- Slant%n");
			System.out.printf("- Small%n");
			System.out.printf("- SmScript%n");
			System.out.printf("- SmShadow%n");
			System.out.printf("- SmSlant%n");
			System.out.printf("- Standard%n");
			System.out.printf("- Terminal%n");

			System.exit(0);
		}

		// name of the font to be used
		final String font;

		// "parse" the command line and set the proper Figlet font name or throw an error
		switch (args[0].trim().toLowerCase()) {
			case "banner":
				font = FigFontResources.BANNER_FLF;
				break;
			case "big":
				font = FigFontResources.BIG_FLF;
				break;
			case "block":
				font = FigFontResources.BLOCK_FLF;
				break;
			case "bubble":
				font = FigFontResources.BUBBLE_FLF;
				break;
			case "digital":
				font = FigFontResources.DIGITAL_FLF;
				break;
			case "ivrit":
				font = FigFontResources.IVRIT_FLF;
				break;
			case "lean":
				font = FigFontResources.LEAN_FLF;
				break;
			case "mini":
				font = FigFontResources.MINI_FLF;
				break;
			case "mnemonic":
				font = FigFontResources.MNEMONIC_FLF;
				break;
			case "script":
				font = FigFontResources.SCRIPT_FLF;
				break;
			case "shadow":
				font = FigFontResources.SHADOW_FLF;
				break;
			case "slant":
				font = FigFontResources.SLANT_FLF;
				break;
			case "small":
				font = FigFontResources.SMALL_FLF;
				break;
			case "smscript":
				font = FigFontResources.SMSCRIPT_FLF;
				break;
			case "smshadow":
				font = FigFontResources.SMSHADOW_FLF;
				break;
			case "smslant":
				font = FigFontResources.SMSLANT_FLF;
				break;
			case "standard":
				font = FigFontResources.STANDARD_FLF;
				break;
			case "terminal":
				font = FigFontResources.TERM_FLF;
				break;
			default:
				throw new IllegalArgumentException("Invalid Figfont: " + args[0]);
		}

		// render to write ASCII-art with the given font
		final FigletRenderer figletRenderer = new FigletRenderer(FigFontResources.loadFigFontResource(font));

		// ASCII-art
		final String output = figletRenderer.renderText("Hello, world!");

		// write to the console
		System.out.printf("%s%n", output);

	}

}
