<?xml version="1.0"?>
<!--
 
 Copyright 2019 University of Padua, Italy
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 
 Author: Nicola Ferro (ferro@dei.unipd.it)
 Version: 1.0
 Since: 1.0
-->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>it.unipd.dei.webapp</groupId>

	<artifactId>hello-quokka-html-css</artifactId>
	
	<version>1.00</version>

	<packaging>war</packaging>

	<!-- Project description elements -->
	<name>Hello Quokka in HTML+CSS</name>

	<description>Basic HTML+CSS page saying about quokkas</description>

	<url>https://bitbucket.org/frrncl/webapp-unipd</url>

	<inceptionYear>2019</inceptionYear>

	<developers>
		<developer>
			<id>nf</id>
			<name>Nicola Ferro</name>
			<email>ferro@dei.unipd.it</email>
			<url>http://www.dei.unipd.it/~ferro/</url>
		</developer>
	</developers>

	<licenses>
		<license>
			<name>The Apache Software License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<organization>
		<name>Department of Information Engineering (DEI), University of Padua, Italy</name>
		<url>http://www.dei.unipd.it/en/</url>
	</organization>

	<!-- Build settings -->
	
	<!-- Specifies the encoding to be used for project source files -->
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>
	
	<!-- Configuration of the default build lifecycle -->
	<build>
		<defaultGoal>compile</defaultGoal>
		
		<!-- source code folder -->
		<sourceDirectory>${basedir}/src/main/java</sourceDirectory>
		
		<!-- compiled code folder -->
		<directory>${basedir}/target</directory>
		
		<!-- name of the generated package -->
		<finalName>${project.artifactId}-${project.version}</finalName>

		<!-- configuration of the plugins for the different goals -->
		<plugins>
		
			<!-- compiler plugin: source and target code is for Java 1.8 -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.8.1</version>
				<configuration>
					<source>17</source>
					<target>17</target>
				</configuration>
			</plugin>
			
			
			<!-- javadoc plugin: output in the javadoc folder -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>3.2.0</version>
				<configuration>
					<reportOutputDirectory>${basedir}/javadoc</reportOutputDirectory>
					<author>true</author>
					<nosince>false</nosince>
					<show>protected</show>
					<source>8</source>
				</configuration>
			</plugin>

			<!-- packager plugin: create a WAR file to be deployed -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<version>3.3.1</version>
				<configuration>
					<webXml>${basedir}/src/main/webapp/WEB-INF/web.xml</webXml>
				</configuration>
			</plugin>

		</plugins>

	</build>

</project>