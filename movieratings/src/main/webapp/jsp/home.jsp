<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="/html/cdn.html"%>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="ISO-8859-1">
    <title>Home</title>
    <script src="${pageContext.request.contextPath}/js/info.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/home.css" type="text/css">
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Rating</a>
            <button
                    class="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarNav"
                    aria-controls="navbarNav"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
            >
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <h1>Welcome back <c:out value="${sessionScope.user.getUsername()}"/></h1>
    <hr/>

    <div>
        <h2>Watch for the first time</h2><br/>
        <div>
            <c:forEach var="movie" items="${movies_wished}" varStatus="status">
                <c:if test="${status.index % 3 == 0}">
                    <div class="row">
                </c:if>
                <div class="col-md-4">
                       <div style="text-align: center">
                           <a href="<c:url value="/movies/info"><c:param name="user" value="${sessionScope.user.getId()}"/><c:param name="movie" value="${movie.getId()}"/></c:url>"><img class="image-lst" src="<c:url value="/logo"><c:param name="movie" value="${movie.getId()}"/></c:url>"  id="img_${movie.getId()}" /></a>
                           <h5><c:out value="${movie.getTitle()}"/></h5>
                           <p><c:out value="${movie.getYear()}"/></p>
                       </div>
                </div>
                <c:if test="${status.index % 3 == 2 or status.last}">
                    </div>
                </c:if>
            </c:forEach>



        </div>
    </div>

    <hr/>
    <div>
        <h2>Just watched</h2>
        <div>
            <c:forEach var="movie" items="${movies_watched}" varStatus="status">
                <c:if test="${status.index % 3 == 0}">
                    <div class="row">
                </c:if>
                <div class="col-md-4">
                    <div style="text-align: center">
                        <a href="<c:url value="/movies/info"><c:param name="user" value="${sessionScope.user.getId()}"/><c:param name="movie" value="${movie.getId()}"/></c:url>"><img class="image-lst" src="<c:url value="/logo"><c:param name="movie" value="${movie.getId()}"/></c:url>"  id="img_${movie.getId()}" /></a>
                        <h5><c:out value="${movie.getTitle()}"/></h5>
                        <p><c:out value="${movie.getYear()}"/></p>
                    </div>
                </div>
                <c:if test="${status.index % 3 == 2 or status.last}">
                    </div>
                </c:if>
            </c:forEach>

        </div>
    </div>
    <c:choose>
        <c:when test="${message.error}">
            <div class="msgmargin">
                <div class="alert alert-danger" role="alert">
                                <span>
                                    <c:out value="${message.message}"/>
                                </span>
                </div>
            </div>
        </c:when>
        <c:otherwise></c:otherwise>
    </c:choose>

</div>
</body>
</html>