<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="/html/cdn.html"%>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="ISO-8859-1">
	<title>Login</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" />
</head>
<body>
<div class="container">
	<div class="sidenav">
		<div class="login-main-text">
			<h2>Movie Ratings<br> Login Page</h2>
			<p>Login in Movie Ratings web app.</p>
		</div>
	</div>
	<div class="main">
		<div class="col-md-6 col-sm-12">
			<div class="login-form">
				<form method="POST" action="<c:url value="/login"/>">
					<div class="form-group login">
						<label>Username</label>
						<input type="text" name="username" class="form-control" placeholder="Username">
					</div>
					<div class="form-group login">
						<label>Password</label>
						<input type="password" name="password" class="form-control" placeholder="Password">
					</div>
					<button type="submit" class="btn btn-black login">Login</button>
				</form>
				<br/>
				<c:choose>
					<c:when test="${message.error}">
						<div class="msgmargin">
							<div class="alert alert-danger" role="alert">
                                <span>
                                    <c:out value="${message.message}"/>
                                </span>
							</div>
						</div>
					</c:when>
					<c:otherwise></c:otherwise>
				</c:choose>

			</div>
		</div>
	</div>




</div>
</body>
</html>