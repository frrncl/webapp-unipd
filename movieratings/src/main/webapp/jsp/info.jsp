<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="/html/cdn.html"%>
    <script type="text/javascript">
        var usr = "${sessionScope.user.getUsername()}";
        console.log(usr)
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="ISO-8859-1">
    <title>Home</title>
    <script src="${pageContext.request.contextPath}/js/info.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/home.css" type="text/css">
</head>
<body>
<div class="container">
    <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Review for: </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="score" class="col-form-label">Score [1-5]:</label>
                            <input type="number" name="score" class="form-control" id="score">
                        </div>
                        <div class="form-group">
                            <label for="comment" class="col-form-label">Comment:</label>
                            <textarea class="form-control" id="comment"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" id="submitreview" class="btn btn-primary">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Rating</a>
            <button
                    class="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarNav"
                    aria-controls="navbarNav"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
            >
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <h1 id="titlefilm"><c:out value="${movieinfo.getTitle()}"/></h1>
    <div>
        <div class="row">
            <div class="col-md-4">
           <img class="image-lst" src="<c:url value="/logo"><c:param name="movie" value="${movieinfo.getId()}"/></c:url>"  id="img_${movieinfo.getId()}" />
            </div>
            <div class="col-md-8">
                <div class = "desc">
                    <b>Year:</b> ${movieinfo.getYear()}
                </div>
                <div class="desc">
                    ${movieinfo.getPlot()}
                </div>
                <div class="desc">
                    <button type="button" id="revbut" class="btn btn-dark">Add Review</button>
                </div>
            <div>
        </div>

    </div>
    <div>
        <h2>Reviews</h2>
        <div id="reviews"></div>
    </div>
            <div id="alerts"></div>

    <c:choose>
        <c:when test="${message.error}">
            <div class="msgmargin">
                <div class="alert alert-danger" role="alert">
                                <span>
                                    <c:out value="${message.message}"/>
                                </span>
                </div>
            </div>
        </c:when>
        <c:otherwise></c:otherwise>
    </c:choose>

</div>
    </div></div>
</body>
</html>