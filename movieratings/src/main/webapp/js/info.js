$(document).ready(function() {
    var username = usr;
    console.log(usr)
    var REVIEWS = [];
    let revs = document.getElementById("reviews");
    const showAlert = (type) => {
        // Create the alert element
        const alertElement = document.createElement("div");
        console.log("type", type);
        if (type === "success") {
            alertElement.classList.add(
                "alert",
                "alert-success",
                "alert-dismissible",
                "fade",
                "show"
            );
            alertElement.role = "alert";
            alertElement.innerHTML = "Successfully removed from list";
        } else {
            alertElement.classList.add(
                "alert",
                "alert-danger",
                "alert-dismissible",
                "fade",
                "show"
            );
            alertElement.role = "alert";
            alertElement.innerHTML = "Something went wrong.";
        }

        // Append the alert element to the document body
        document.getElementById("alerts").appendChild(alertElement);
        // Set a timeout to remove the alert after 2 seconds
        setTimeout(() => {
            alertElement.remove();
        }, 5000);
    };
    async function logReviews() {
        //var img = document.getElementsByClassName("image-lst");
        //var id = img.item(0).id.split("img_")[1]
        var id = window.location.href.split('movie=')[1]
        console.log(id)
        var url = "http://localhost:8081/rating0/rest/reviews/"+ id
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            let items = data['resource-list'];
            console.log("items", items);
            REVIEWS = items.map(i=>i);
            items.map((rev,index)=>{
                add_review(rev,index)

            })
        } else {

            showAlert("danger");
            console.log(response);
        }
    }


    logReviews();

    function add_review(rev,index){
        let div = document.createElement("div")
        div.className = "row";
        let div_col = document.createElement("div")
        if(parseInt(rev.score) > 0){
            div_col.className = "col-md-3";
            for (let i = 0; i < parseInt(rev.score); i++) {
                const star = document.createElement("i");
                star.classList.add("fas", "fa-star");

                div_col.appendChild(star);
            }
            div.appendChild(div_col)
            let div_col_9 = document.createElement("div")
            div_col_9.className = "col-md-9";
            let div_col_rev = document.createElement("div")
            div_col_rev.id = rev['uid']
            let div_col_9_user = document.createElement("div")
            let div_col_9_comment = document.createElement("div")
            div_col_9_user.textContent = rev.username
            div_col_9_user.style.fontWeight = "bold";
            if(rev.comment !== null){
                div_col_9_comment.textContent = rev.comment
            }else{
                div_col_9_comment.textContent = ""
            }
            div_col_9.appendChild(div_col_9_user)
            div_col_9.appendChild(div_col_9_comment)
            div.appendChild(div_col_9)
            div_col_rev.appendChild(div)
            revs.appendChild(div_col_rev)

        }
    }
    var modal = document.getElementById("exampleModalLabel");
    modal.textContent = "New Review for: " + document.getElementById("titlefilm").textContent

    let revbut = document.getElementById("revbut");
    revbut.addEventListener("click", function () {
        var modal = document.getElementById("formModal");
        var bsModal = new bootstrap.Modal(modal);
        bsModal.show();
    });

    document.getElementById("submitreview").addEventListener("click",function (e){
        e.preventDefault()
        var score = document.getElementById("score").value
        var comment = document.getElementById("comment").value
        console.log(score)
        console.log(comment)
        var xhr = new XMLHttpRequest();
        var mid = window.location.href.split('movie=')[1]
        var uid = window.location.href.split('user=')[1].split('&')[0]
        var datajson = {"review":{"score":score,"comment":comment,"mid":mid,"uid":uid}}
        var data = JSON.stringify(datajson)

        var url = "http://localhost:8081/rating0/rest/reviews/"
        xhr.open('POST', url, true);
        xhr.setRequestHeader('Content-Type', 'application/json');

        xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    // Request was successful, handle the response here
                    console.log('Response:', xhr.responseText);
                    REVIEWS = REVIEWS.filter(x=> x['uid'] !== uid)

                    REVIEWS.push(data)
                    var myModal = document.getElementById('formModal');
                    var modal = new bootstrap.Modal(myModal);
                    $("#formModal .close").click()
                    document.getElementById(uid).remove()
                    datajson['review']['username'] = username;
                    add_review(datajson['review'],0)


                } else {
                    // Request failed
                    console.error('Request failed:', xhr.status);
                }
            }
        };

        xhr.send(data);


    })



})





