var contextPath = 'https://exa.dei.unipd.it/crane';
// var contextPath = 'http://localhost:8080/Students_WebApp-1.0';

function sanitize(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

// const validateEmail = (email) => {
//     return String(email)
//         .toLowerCase()
//         .match(
//             /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
//         );
// };

$(document).ready(function() {
    let selectMastterdegrees = document.getElementById('degreebox')
    var urlMd = new URL(contextPath+"/masterdegree/list")
    var urlCourses = new URL(contextPath+"/courses/list")
    console.log(urlCourses,urlMd)
    // uncomment to get rid of filling jsp based
    // getMesterDegress(urlMd)
    // getCourses(urlCourses)

    const togglePassword = document.getElementById('togglePassword');
    let password = document.getElementById('password');


    togglePassword.addEventListener(
            "click",
            function() {
                // toggle the type attribute
                let type = password.getAttribute("type") === "password" ? "text"  : "password";
                console.log('type',type)
                password.setAttribute("type", type);

                if (this.classList.contains("fa-eye-slash")) {
                    this.classList.replace("fa-eye-slash",
                        "fa-eye");
                } else {
                    this.classList.replace("fa-eye",
                        "fa-eye-slash");
                }
            });


} )

function getMesterDegress(url){
    // console.log('cerco i md')
    $.ajax({
        url:url,
        type: "GET",
        contentType: 'application/json',
        dataType: 'json',
        success: function (data, status, request) {
            console.log('data',data)

            // var jsonData = JSON.parse(data);
            var mgLst = data['data']['mg-names-list'];

            let selectMastterdegrees = document.getElementById('degreebox')
            var option = document.createElement("option");
            option.text = "";
            option.value = "";
            selectMastterdegrees.appendChild(option);
            console.log(selectMastterdegrees)
            for (let i=0; i<mgLst.length; i++) {
                let name = sanitize(mgLst[i]['name']);
                let id = sanitize(mgLst[i]['id']);
                var option = document.createElement("option");
                option.text = name;
                option.value = id;
                selectMastterdegrees.appendChild(option);
            }
        },
        error: function (request, status, error) {
            console.error(request, error);

        }
    });

}

function getCourses(url){
    console.log('cerco i corsi')

    $.ajax({
        url:url,
        type: "GET",
        contentType: 'application/json',
        dataType: 'json',
        success: function (data, status, request) {
            // var jsonData = JSON.parse(data);
            var mgLst = data['data']['courses-names-list'];

            let selectCourses = document.getElementById('coursesbox')
            var option = document.createElement("option");
            option.text = "";
            option.value = "";
            selectCourses.appendChild(option);

            for (let i=0; i<mgLst.length; i++) {
                let name = sanitize(mgLst[i]['name']);
                let id = sanitize(mgLst[i]['id']);

                var option = document.createElement("option");
                option.text = name;
                option.value = id;
                selectCourses.appendChild(option);
            }
        },
        error: function (request, status, error) {
            console.error(request, error);

        }
    });


}
