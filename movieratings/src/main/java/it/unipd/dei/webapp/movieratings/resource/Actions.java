package it.unipd.dei.webapp.movieratings.resource;


/**
 * Contains constants for the actions performed by the application.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public final class Actions {

	public static final String GET_REVIEWS_BY_ID = "STATEMENT_GET_RATINGS";

	public static final String ADD_REVIEW = "STATEMENT_POST_REVIEW";
	public static final String LOAD_FILM_PHOTO = "LOAD_FILM_PHOTO";


	private Actions() {
		throw new AssertionError(String.format("No instances of %s allowed.", Actions.class.getName()));
	}
}