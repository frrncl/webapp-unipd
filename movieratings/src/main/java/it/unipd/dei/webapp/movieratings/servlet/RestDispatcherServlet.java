package it.unipd.dei.webapp.movieratings.servlet;

import it.unipd.dei.webapp.movieratings.resource.*;
import it.unipd.dei.webapp.movieratings.rest.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.OutputStream;


public final class RestDispatcherServlet extends AbstractDatabaseServlet {

    /**
     * The JSON UTF-8 MIME media type
     */
    private static final String JSON_UTF_8_MEDIA_TYPE = "application/json; charset=utf-8";

    @Override
    protected void service(final HttpServletRequest req, final HttpServletResponse res) throws IOException {

        LogContext.setIPAddress(req.getRemoteAddr());

        final OutputStream out = res.getOutputStream();

        try {

            // if the requested resource was an Student, delegate its processing and return
            if (processReview(req, res)) {
                return;
            }


            // if none of the above process methods succeeds, it means an unknown resource has been requested
            LOGGER.warn("Unknown resource requested: %s.", req.getRequestURI());

            final Message m = new Message("Unknown resource requested.", "E4A6",
                    String.format("Requested resource is %s.", req.getRequestURI()));
            res.setStatus(HttpServletResponse.SC_NOT_FOUND);
            res.setContentType(JSON_UTF_8_MEDIA_TYPE);
            m.toJSON(out);
        } catch (Throwable t) {
            LOGGER.error("Unexpected error while processing the REST resource.", t);

            final Message m = new Message("Unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(out);
        } finally {

            // ensure to always flush and close the output stream
            if (out != null) {
                out.flush();
                out.close();
            }

            LogContext.removeIPAddress();
        }
    }

    private boolean processReview(final HttpServletRequest req, final HttpServletResponse res) throws Exception {

        final String method = req.getMethod();
        String path = req.getRequestURI();
        LOGGER.info(path);

        Message m = null;

        if (path.lastIndexOf("reviews") <= 0) {
            return false;
        }

        path = path.substring(path.lastIndexOf("reviews") + 8);
        LOGGER.info(path);



        // the request URI is: /student
        // if method GET, list students
        if (path.length() > 0) {

            switch (method) {
                case "GET":
                    new SearchReviewsByMovieIDRR(req, res, getConnection()).serve();
                    break;
                default:
                    LOGGER.warn("Unsupported operation for URI /rest/reviews/{id}: %s.", method);

                    m = new Message("Unsupported operation for URI /reviews.", "E4A5",
                            String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                    m.toJSON(res.getOutputStream());
                    break;
            }

        } else {

            switch (method) {
                case "POST":
                    new AddReviewRR(req, res, getConnection()).serve();

                    break;
                default:
                    LOGGER.warn("Unsupported operation for URI /reviews: %s.", method);

                    m = new Message("Unsupported operation for URI /reviews.", "E4A5",
                            String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                    m.toJSON(res.getOutputStream());
                    break;
            }



        }


        return true;

    }
}