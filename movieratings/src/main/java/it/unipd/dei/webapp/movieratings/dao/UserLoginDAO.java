package it.unipd.dei.webapp.movieratings.dao;

import it.unipd.dei.webapp.movieratings.resource.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;


public class UserLoginDAO extends AbstractDAO<User>{
    private static final String STATEMENT_LOGIN = "SELECT id,username,password from public.user where username = ? and password = md5(?)";


    private final User user;

    public UserLoginDAO(final Connection con,User user) {
        super(con);
        this.user = user;
    }

    @Override
    protected void doAccess() throws Exception {

        PreparedStatement stmnt = null;
        ResultSet rs = null;
        User user_to_ret = null;

        // the results of the search
        try {
            stmnt = con.prepareStatement(STATEMENT_LOGIN);
            stmnt.setString(2, user.getPassword());
            stmnt.setString(1, user.getUsername());

            rs = stmnt.executeQuery();



            if(rs.next()){
                user_to_ret = new User(rs.getLong("id"),rs.getString("username"),rs.getString("password"));
            }


        } finally {
            if (rs != null) {
                rs.close();
            }

            if (stmnt != null) {
                stmnt.close();
            }

        }
        this.outputParam = user_to_ret;

    }
}


