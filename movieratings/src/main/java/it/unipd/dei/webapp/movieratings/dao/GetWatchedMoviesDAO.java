package it.unipd.dei.webapp.movieratings.dao;

import it.unipd.dei.webapp.movieratings.resource.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;


public class GetWatchedMoviesDAO extends AbstractDAO<ArrayList<Movie>>{
    private static final String STATEMENT_WATCHED = "select mid,title,plot,logo,logoMediaType,year from movie as m inner join user_rate_movie as u on u.mid = m.id where uid = ?";


    private final User user;

    public GetWatchedMoviesDAO(final Connection con, User user) {
        super(con);
        this.user = user;
    }

    @Override
    protected void doAccess() throws Exception {

        PreparedStatement stmnt = null;
        ResultSet rs = null;
        ArrayList<Movie> movies = new ArrayList<>();

        // the results of the search
        try {
            stmnt = con.prepareStatement(STATEMENT_WATCHED);
            stmnt.setLong(1, user.getId());

            rs = stmnt.executeQuery();



            while(rs.next()){
                Movie movie = new Movie(rs.getLong("mid"),rs.getString("title"),rs.getString("plot"), null,null,rs.getString("year"));
                movies.add(movie);
            }


        } finally {
            if (rs != null) {
                rs.close();
            }

            if (stmnt != null) {
                stmnt.close();
            }

        }
        this.outputParam = movies;

    }
}


