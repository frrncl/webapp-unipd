package it.unipd.dei.webapp.movieratings.dao;

import it.unipd.dei.webapp.movieratings.resource.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;


public class GetMovieRatingsDAO extends AbstractDAO<ArrayList<Review>>{
    private static final String STATEMENT_GET_RATINGS = "select username,m.id as uid,mid,score,comment from public.user as m inner join user_rate_movie as u on u.uid = m.id where mid = ?";


    private final Long mid;

    public GetMovieRatingsDAO(final Connection con, Long mid) {
        super(con);
        this.mid = mid;
    }

    @Override
    protected void doAccess() throws Exception {

        PreparedStatement stmnt = null;
        ResultSet rs = null;
        ArrayList<Review> rev = new ArrayList<>();

        // the results of the search
        try {
            stmnt = con.prepareStatement(STATEMENT_GET_RATINGS);
            stmnt.setLong(1, mid);
            rs = stmnt.executeQuery();



            while(rs.next()){
                Review review = new Review(rs.getLong("mid"),rs.getLong("uid"),rs.getString("username"), rs.getString("comment"),rs.getInt("score"));
                rev.add(review);
            }


        } finally {
            if (rs != null) {
                rs.close();
            }

            if (stmnt != null) {
                stmnt.close();
            }

        }
        this.outputParam = rev;

    }
}


