package it.unipd.dei.webapp.movieratings.resource;

import org.json.JSONObject;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import org.json.JSONObject;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

public class Review  extends AbstractResource {
    private final String username;
    private final Long mid;
    private final Long uid;
    private final String comment;
    private final int score;

    // this is used to return a student after login
    public Review(Long mid, Long uid, String username, String comment, int score){
        this.mid = mid;
        this.uid = uid;
        this.username = username;
        this.comment = comment;
        this.score = score;
    }

    public Long getMid() {
        return mid;
    }
    public Long getUid() {
        return uid;
    }
    public int getScore() {
        return score;
    }
    public String getComment() {
        return comment;
    }
    public String getUsername() {
        return username;
    }


    public JSONObject toJSON(){
        JSONObject uJson = new JSONObject();
        uJson.put("mid", mid);
        uJson.put("uid", uid);
        uJson.put("username", username);
        uJson.put("comment", comment);
        uJson.put("score", score);
        return uJson;
    }

    @Override
    protected void writeJSON(final OutputStream out) throws IOException {

        final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

        jg.writeStartObject();


        jg.writeNumberField("uid", uid);
        jg.writeNumberField("mid", mid);
        jg.writeStringField("username", username);
        jg.writeStringField("score", String.valueOf(score));
        jg.writeStringField("comment", comment);

        jg.writeEndObject();


        jg.flush();
    }

    public static Review fromJSON(final InputStream in) throws IOException  {

        // the fields read from JSON
        int score = 0;
        String comment = "";
        Long mid = null;
        Long uid = null;
        String username = "";



        try {
            final JsonParser jp = JSON_FACTORY.createParser(in);
            // while we are not on the start of an element or the element is not
            // a token element, advance to the next element (if any)
            while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !"review".equals(jp.getCurrentName())) {

                // there are no more events
                if (jp.nextToken() == null) {
                    LOGGER.error("No student object found in the stream.");
                    throw new EOFException("Unable to parse JSON: no student object found.");
                }
            }

            while (jp.nextToken() != JsonToken.END_OBJECT) {

                if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

                    switch (jp.getCurrentName()) {
                        case "mid":
                            jp.nextToken();
                            mid = Long.parseLong(jp.getText());
                            break;
                        case "uid":
                            jp.nextToken();
                            uid = Long.parseLong(jp.getText());
                            break;
                        case "username":
                            jp.nextToken();
                            username = jp.getText();
                            break;
                        case "comment":
                            jp.nextToken();
                            comment = jp.getText();
                            break;
                        case "score":
                            jp.nextToken();
                            score = Integer.parseInt(jp.getText());
                            break;

                    }
                }
            }
        } catch(IOException e) {
            LOGGER.error("Unable to parse a Review object from JSON.", e);
            throw e;
        }
        LOGGER.info(mid);
        LOGGER.info(uid);
        LOGGER.info(score);
        LOGGER.info(comment);
        return new Review(mid, uid, username, comment, score);
    }

}
