/*
 * Copyright 2023 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.movieratings.dao;
import it.unipd.dei.webapp.movieratings.resource.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Loads the photo of an Employee.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class LoadingImageDAO extends AbstractDAO<Movie> {

    /**
     * The SQL statement to be executed
     */
    private static final String LOAD_FILM_PHOTO = "SELECT id,title,plot,logo,logoMediaType,year FROM movie WHERE id = ?";

    /**
     * The nane of the group
     */
    private final Long id;


    public LoadingImageDAO(final Connection con, final Long id) {
        super(con);
        this.id = id;
    }

    @Override
    public final void doAccess() throws SQLException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Movie movie = null;

        // the results of the search

        try {
            pstmt = con.prepareStatement(LOAD_FILM_PHOTO);

            pstmt.setLong(1, id);


            rs = pstmt.executeQuery();

            if (rs.next()) {

                movie = new Movie(rs.getLong("id"),rs.getString("title"),rs.getString("plot"),rs.getBytes("logo"),rs.getString("logoMediaType"),rs.getString("year"));

                LOGGER.info("Photo for movie {} successfully loaded.", id);
            } else {
                throw new SQLException(String.format("Image %d not found.", id), "NOT_FOUND");
            }


        } finally {
            if (rs != null) {
                rs.close();
            }

            if (pstmt != null) {
                pstmt.close();
            }

        }

        this.outputParam = movie;
    }
}
