package it.unipd.dei.webapp.movieratings.servlet;

import it.unipd.dei.webapp.movieratings.utils.ErrorCode;
import it.unipd.dei.webapp.movieratings.dao.*;
import it.unipd.dei.webapp.movieratings.resource.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;


@WebServlet(name = "MovieServlet", value = "/movies/info/*")
public class MovieServlet extends AbstractDatabaseServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // get both student and group info
        LogContext.setIPAddress(request.getRemoteAddr());
        HttpSession session = request.getSession();
        String op = request.getParameter("movie");

        try {
            if (session.getAttribute("user") == null){
                Message m = new Message(
                        "User not login",
                        "E400", "User not found");
                ErrorCode ec = ErrorCode.INTERNAL_ERROR;
                LOGGER.error("An error occurred, no user logged in");

                response.setStatus(ec.getHTTPCode());
                request.setAttribute("message", m);
                request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);

            }else if (op.length() == 0){
                Message m = new Message(
                        "An error occurred",
                        "E400", "User not found");
                ErrorCode ec = ErrorCode.INTERNAL_ERROR;
                LOGGER.error("An error occurred, no movie provided");
                response.setStatus(ec.getHTTPCode());
                request.setAttribute("message", m);
                request.getRequestDispatcher("/jsp/home.jsp").forward(request, response);
            }

            else{
                Long mid = Long.parseLong(request.getParameter("movie"));
                Movie movie = new MovieInfoDAO(getConnection(),mid).access().getOutputParam();
                if(movie == null){
                    Message m = new Message(
                            "An error occurred",
                            "E400", "User not found");
                    ErrorCode ec = ErrorCode.INTERNAL_ERROR;
                    response.setStatus(ec.getHTTPCode());
                    request.setAttribute("message", m);
                    request.getRequestDispatcher("/jsp/home.jsp").forward(request, response);
                }
                else{
                    request.setAttribute("movieinfo",movie);
                    request.getRequestDispatcher("/jsp/info.jsp").forward(request,response);
                }
            }


        } catch (SQLException e) {
            Message m = new Message(
                    "An error occurred.",
                    "E400", e.getMessage());
            request.setAttribute("message",m);

            request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);

        }
    }


}
