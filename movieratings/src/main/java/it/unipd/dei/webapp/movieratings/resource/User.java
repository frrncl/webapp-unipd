package it.unipd.dei.webapp.movieratings.resource;

import org.json.JSONObject;

public class User {

    private final Long id;
    private final String username;
    private final String password;
    public User(Long id,String username, String password){
        this.id = id;
        this.username = username;
        this.password = password;

    }
    public final String getUsername() {
        return username;
    }
    public final Long getId() {
        return id;
    }
    public final String getPassword() {
        return password;
    }


}
