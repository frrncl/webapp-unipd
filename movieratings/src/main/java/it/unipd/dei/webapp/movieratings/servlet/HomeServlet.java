package it.unipd.dei.webapp.movieratings.servlet;

import it.unipd.dei.webapp.movieratings.dao.*;
import it.unipd.dei.webapp.movieratings.resource.*;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import it.unipd.dei.webapp.movieratings.utils.ErrorCode;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;


@WebServlet(name = "HomeServlet", value = "/home")
public class HomeServlet extends AbstractDatabaseServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // get both student and group info
        LogContext.setIPAddress(request.getRemoteAddr());
        HttpSession session = request.getSession(false);

        try {
            if (session.getAttribute("user") == null){
                Message m = new Message(
                        "User not login",
                        "E400", "User not found");
                ErrorCode ec = ErrorCode.WRONG_GROUP_INFO;
                response.setStatus(ec.getHTTPCode());
                request.setAttribute("message", m);
                request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);

            }else{
                User user = (User) session.getAttribute("user");
                ArrayList<Movie> movies_watched = new GetWatchedMoviesDAO(getConnection(),user).access().getOutputParam();
                ArrayList<Movie> movies_wished = new GetWishMoviesDAO(getConnection(),user).access().getOutputParam();
                LOGGER.info(movies_wished);
                request.setAttribute("movies_watched",movies_watched);
                request.setAttribute("movies_wished",movies_wished);
                request.getRequestDispatcher("/jsp/home.jsp").forward(request, response);

            }

        } catch (SQLException e) {
            Message m = new Message(
                    "An error occurred.",
                    "E400", e.getMessage());

            ErrorCode ec = ErrorCode.INTERNAL_ERROR;
            response.setStatus(ec.getHTTPCode());
            request.setAttribute("message", m);
            request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);

        }
    }


}
