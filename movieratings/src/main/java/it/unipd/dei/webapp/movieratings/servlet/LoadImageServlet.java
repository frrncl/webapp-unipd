/*
 * Copyright 2023 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.movieratings.servlet;
import it.unipd.dei.webapp.movieratings.dao.*;
import it.unipd.dei.webapp.movieratings.resource.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * Loads the photo of an employee.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */

@WebServlet(name = "LoadImageServlet", value = "/logo/*")
public final class LoadImageServlet extends AbstractDatabaseServlet {

    /**
     * Loads the photo of an employee.
     *
     * @param req the HTTP request from the client.
     * @param res the HTTP response from the server.
     *
     * @throws ServletException if any error occurs while executing the servlet.
     * @throws IOException      if any error occurs in the client/server communication.
     */
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        LogContext.setIPAddress(req.getRemoteAddr());
        LogContext.setAction(Actions.LOAD_FILM_PHOTO);

        // request parameter
        int badge = -1;

        // model
        Movie e = null;

        try {

            // retrieves the request parameter
            String op = req.getParameter("movie");

            // creates a new object for accessing the database and loading the photo of an employees
            e = new LoadingImageDAO(getConnection(), Long.parseLong(op)).access().getOutputParam();

            if (e.getLogo() != null) {
                res.setContentType(e.getLogoMediaType());
                res.getOutputStream().write(e.getLogo());
                res.getOutputStream().flush();

                LOGGER.info("Photo for movie %d successfully sent.", op);
            } else {
                LOGGER.info("Movie %d has no logo.", op);

                res.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }

        } catch (Exception ex) {
            LOGGER.error("Unable to load the photo of the employee.", ex);

            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } finally {
            LogContext.removeIPAddress();
            LogContext.removeAction();
            LogContext.removeUser();
        }

    }

}
