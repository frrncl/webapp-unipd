package it.unipd.dei.webapp.movieratings.servlet;

import it.unipd.dei.webapp.movieratings.resource.*;
import it.unipd.dei.webapp.movieratings.dao.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import it.unipd.dei.webapp.movieratings.utils.ErrorCode;

import java.io.IOException;
import java.sql.SQLException;

public class LoginServlet extends AbstractDatabaseServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("jsp/login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //take the request uri
        LogContext.setIPAddress(request.getRemoteAddr());
        LogContext.setResource(request.getRequestURI());
        String op = request.getRequestURI();

        //remove everything prior to /user (included) and use the remainder as
        //indicator for the required operation
        op = op.substring(op.lastIndexOf("/") + 1);

        switch (op){

            case "login":
                // the requested operation is login
                LogContext.setAction("login");

                loginOperations(request, response, false);
                break;


            default:
                Message m = new Message("An error occurred default","E200","Operation Unknown");
                request.setAttribute("message",m);
                request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);

        }
    }


    public void loginOperations(HttpServletRequest request, HttpServletResponse response, boolean isValid) throws ServletException, IOException {

        User user = null;
        Message m = null;
        HttpSession session = request.getSession(false);
        // Get the current session, don't create if it doesn't exist
        if (session != null) {
            session.invalidate(); // Invalidate the current session
        }

        session = request.getSession(true);

        String regex_psw = "^(?=.*[A-Z])(?=.*[0-9]).{8,}$";
        String regex_username  = "^(?=.*[A-Z]).{1,}$";

        try {
            //take from the request, the parameters (email and password, course and masterdegree)
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            if (username == null || username.equals("") || !username.matches(regex_username)){
                m = new Message("Insert a valid usernmae","E200","Missing fields");
                ErrorCode ec = ErrorCode.WRONG_CREDENTIALS;
                response.setStatus(ec.getHTTPCode());
                request.setAttribute("message", m);
                request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
            }
            else if (password == null || password.equals("") || !password.matches(regex_psw)){
                m = new Message("Insert a valid password","E200","Missing fields");
                ErrorCode ec = ErrorCode.WRONG_CREDENTIALS;
                response.setStatus(ec.getHTTPCode());
                request.setAttribute("message", m);
                request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
            }
            else{
                user = new User(null,username,password);
                user = new UserLoginDAO(getConnection(),user).access().getOutputParam();
                if(user == null){
                    m = new Message("The user does not exist","E200","User not found");
                    ErrorCode ec = ErrorCode.USER_NOT_FOUND;
                    response.setStatus(ec.getHTTPCode());
                    request.setAttribute("message", m);
                    request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
                }else{
                    session.setAttribute("user", user);
                    response.sendRedirect(request.getContextPath()+"/home");
                }

            }





        } catch (SQLException e){
            m = new Message("An error occurred SQL","E200",e.getMessage());
            request.setAttribute("message", m);
            request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
        }
        catch (NumberFormatException e){
            m = new Message("An error occurred handling numbers","E200",e.getMessage());
            request.setAttribute("message", m);
            request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
        }


    }
}
