package it.unipd.dei.webapp.movieratings.dao;

import it.unipd.dei.webapp.movieratings.resource.Movie;
import it.unipd.dei.webapp.movieratings.resource.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;


public class GetWishMoviesDAO extends AbstractDAO<ArrayList<Movie>>{
    private static final String STATEMENT_WISH_LIST = "select mid,title,plot,year from movie as m inner join user_watch_movie as u on u.mid = m.id where uid = ?";


    private final User user;

    public GetWishMoviesDAO(final Connection con, User user) {
        super(con);
        this.user = user;
    }

    @Override
    protected void doAccess() throws Exception {

        PreparedStatement stmnt = null;
        ResultSet rs = null;
        ArrayList<Movie> movies = new ArrayList<>();

        // the results of the search
        try {
            stmnt = con.prepareStatement(STATEMENT_WISH_LIST);
            stmnt.setLong(1, user.getId());

            rs = stmnt.executeQuery();



            while(rs.next()){
                Movie movie = new Movie(rs.getLong("mid"),rs.getString("title"),rs.getString("plot"),null,null,rs.getString("year"));
                movies.add(movie);
                LOGGER.info(movie.getTitle());
            }
            LOGGER.info(movies);



        } finally {
            if (rs != null) {
                rs.close();
            }

            if (stmnt != null) {
                stmnt.close();
            }

        }
        this.outputParam = movies;

    }
}


