package it.unipd.dei.webapp.movieratings.dao;

import it.unipd.dei.webapp.movieratings.resource.Movie;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class MovieInfoDAO extends AbstractDAO<Movie> {
    private static final String STATEMENT_MOVIE_INFO = "SELECT id,title,plot,year FROM public.movie where id = ?";
    private final Long mid;
    public MovieInfoDAO(final Connection con, final Long mid) {
        super(con);
        this.mid = mid;
    }
    @Override
    protected void doAccess() throws Exception {

        PreparedStatement stmnt = null;
        ResultSet rs = null;
        Movie movie = null;
        try {
            stmnt = con.prepareStatement(STATEMENT_MOVIE_INFO);
            stmnt.setLong(1, mid);
            rs = stmnt.executeQuery();
            if(rs.next()){
                movie = new Movie(rs.getLong("id"),rs.getString("title"),rs.getString("plot"), null,null,rs.getString("year"));
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmnt != null) {
                stmnt.close();
            }
        }
        this.outputParam = movie;

    }
}
