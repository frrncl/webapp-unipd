package it.unipd.dei.webapp.movieratings.rest;

import it.unipd.dei.webapp.movieratings.dao.*;
import it.unipd.dei.webapp.movieratings.resource.*;
import it.unipd.dei.webapp.movieratings.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * A REST resource for searching {@link Student}s by salary.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class AddReviewRR extends AbstractRR {

    /**
     * Creates a new REST resource for searching {@code Student}s by salary.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @param con the connection to the database.
     */
    public AddReviewRR(final HttpServletRequest req, final HttpServletResponse res, Connection con) {
        super(Actions.ADD_REVIEW, req, res, con);
    }


    @Override
    protected void doServe() throws IOException {

        Integer el = null;
        Message m = null;

        try {
            final Review review = Review.fromJSON(req.getInputStream());


            // creates a new DAO for accessing the database and stores the employee
            el = new PostReviewDAO(con, review).access().getOutputParam();

            // creates a new DAO for accessing the database and searches the Student(s)

            if (el == 1 ) {
                LOGGER.info("Review for the movie %d successfully added",review.getMid());

                res.setStatus(HttpServletResponse.SC_OK);
                review.toJSON(res.getOutputStream());
            }

            else { // it should not happen
                LOGGER.error("Fatal error while adding review(s)");

                m = new Message("Student not found.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (IndexOutOfBoundsException | NumberFormatException ex) {
            LOGGER.warn("Cannot search Student(s): wrong format for URI .", ex);

            m = new Message("Cannot search Student(s): wrong format for URI.", "E4A7",
                    ex.getMessage());
            res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            m.toJSON(res.getOutputStream());
        } catch (SQLException ex) {
            LOGGER.error("Cannot add the Student(s): unexpected database error.", ex);

            m = new Message("Cannot add the Student(s): unexpected database error.", "E5A1", ex.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }


}
