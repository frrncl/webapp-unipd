package it.unipd.dei.webapp.movieratings.rest;


import it.unipd.dei.webapp.movieratings.resource.*;
import it.unipd.dei.webapp.movieratings.dao.*;
import it.unipd.dei.webapp.movieratings.servlet.LogContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;


public final class SearchReviewsByMovieIDRR extends AbstractRR {

    public SearchReviewsByMovieIDRR(final HttpServletRequest req, final HttpServletResponse res, Connection con) {
        super(Actions.GET_REVIEWS_BY_ID, req, res, con);
    }


    @Override
    protected void doServe() throws IOException {

        ArrayList<Review> reviews;
        Message m;

        try {
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("reviews") + 8);

            final Long mid = Long.parseLong(path);

            LogContext.setResource(path);
            reviews = new GetMovieRatingsDAO(con,mid).access().getOutputParam();
            LOGGER.info("moview",mid);

            if (reviews != null) {
                LOGGER.info("Reviews %d successfully retrieved",mid);

                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(reviews).toJSON(res.getOutputStream());
            } else { // it should not happen
                LOGGER.error("Fatal error while searching review(s) %d.",mid);

                m = new Message("Movie not found.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (IndexOutOfBoundsException | NumberFormatException ex) {
            LOGGER.warn("Cannot search Movie(s): wrong format for URI /movies/{mid}.", ex);

            m = new Message("Cannot search Student(s): wrong format for URI /movies/{mid}.", "E4A7",
                    ex.getMessage());
            res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            m.toJSON(res.getOutputStream());
        } catch (SQLException ex) {
            LOGGER.error("Cannot search Review(s): unexpected database error.", ex);

            m = new Message("Cannot search Review(s): unexpected database error.", "E5A1", ex.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }


}
