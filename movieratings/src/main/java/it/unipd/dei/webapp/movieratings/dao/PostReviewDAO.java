package it.unipd.dei.webapp.movieratings.dao;

import it.unipd.dei.webapp.movieratings.resource.Movie;
import it.unipd.dei.webapp.movieratings.resource.Review;
import it.unipd.dei.webapp.movieratings.resource.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;


public class PostReviewDAO extends AbstractDAO<Integer>{
    private static final String STATEMENT_POST_REVIEW = "update user_rate_movie set score = ?, comment = ?, rate_time = NOW() where uid = ? and mid = ? RETURNING *";


    private final Review review;

    public PostReviewDAO(final Connection con, Review review) {
        super(con);
        this.review = review;
    }

    @Override
    protected void doAccess() throws Exception {

        PreparedStatement stmnt = null;
        ResultSet rs = null;
        ArrayList<Movie> movies = new ArrayList<>();
        Integer intero = null;
        // the results of the search
        try {
            stmnt = con.prepareStatement(STATEMENT_POST_REVIEW);
            stmnt.setInt(1, review.getScore());
            stmnt.setString(2,review.getComment());
            stmnt.setLong(3,review.getUid());
            stmnt.setLong(4,review.getMid());

            rs = stmnt.executeQuery();


            if(rs.next()){
                ;intero = 1;
            }



        } finally {
            if (rs != null) {
                rs.close();
            }

            if (stmnt != null) {
                stmnt.close();
            }

        }
        this.outputParam = intero;

    }
}


