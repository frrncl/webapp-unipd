package it.unipd.dei.webapp.movieratings.resource;

import org.json.JSONObject;

public class Movie {
    private final Long id;
    private final String title;
    private final String year;
    private final String plot;
    private final byte[] logo;
    private  final String logoMediaType;


    // this is used to return a student after login
    public Movie(Long id, String title, String plot, byte[] logo, String logoMediaType, String year){
        this.id = id;
        this.title = title;
        this.plot = plot;
        this.logo = logo;
        this.year = year;
        this.logoMediaType = logoMediaType;

    }

    public Long getId() {
        return id;
    }
    public String getYear() {
        return year;
    }

    public String getPlot() {
        return plot;
    }
    public String getTitle() {
        return title;
    }


    public final byte[] getLogo() {
        return logo;
    }
    public final String getLogoMediaType() {
        return logoMediaType;
    }


}
