--  Copyright 2018-2019 University of Padua, Italy
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      http://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  
--  Author: Nicola Ferro (ferro@dei.unipd.it)
--  		Maria Maistro (maistro@dei.unipd.it)
--  Version: 1.0
--  Since: 1.0


-- #################################################################################################
-- ## Creation of a schema to avoid name clashes                                                  ##
-- #################################################################################################

-- Drop the MyToys schema, if exists, and any object within it
DROP SCHEMA IF EXISTS MyToys CASCADE;

-- Create the MyToys schema
CREATE SCHEMA MyToys;
COMMENT ON SCHEMA MyToys IS 'Schema for containing the objects of the MyToys Web application for exam simulation';


-- #################################################################################################
-- ## Creation of enumerations                                                                    ##
-- #################################################################################################

--
-- This enumeration represents the categories of the possible types of products of the MyToys company.
--
-- Version 1.00

CREATE TYPE MyToys.Category AS ENUM (
	'Animal', 
	'Car', 
	'Doll',
	'Lego',
	'Toy Soldier'
);

COMMENT ON TYPE MyToys.Category IS 'The categories of the possible types of products of the MyToys company.';


-- #################################################################################################
-- ## Creation of the tables                                                                      ##
-- #################################################################################################

--
-- This table represents the catalog of products sold by the MyToys company
--
-- Version 1.00
CREATE TABLE MyToys.Product (
	id SERIAL,
	category MyToys.Category NOT NULL,
	name TEXT NOT NULL,
	ageRange TEXT,
	price DOUBLE PRECISION NOT NULL CHECK(price > 0),
	PRIMARY KEY (id)
);

COMMENT ON TABLE MyToys.Product IS 'The catalog of products sold by the MyToys company.';
COMMENT ON COLUMN MyToys.Product.id IS 'The unique identifier of a product.';
COMMENT ON COLUMN MyToys.Product.category IS 'The type of a product.';
COMMENT ON COLUMN MyToys.Product.name IS 'The name of a product.';
COMMENT ON COLUMN MyToys.Product.ageRange IS 'The range of ages for which a product is suitable.';
COMMENT ON COLUMN MyToys.Product.price IS 'The price of a product in euros.';
