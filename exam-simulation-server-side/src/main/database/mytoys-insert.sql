--  Copyright 2018-2019 University of Padua, Italy
--  
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--  
--      http://www.apache.org/licenses/LICENSE-2.0
--  
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--  
--  Author: Nicola Ferro (ferro@dei.unipd.it)
--  		Maria Maistro (maistro@dei.unipd.it)
--  Version: 1.0
--  Since: 1.0
--
-- Insertion into the Product table
--
INSERT INTO MyToys.Product(id, category, name, ageRange, price) VALUES (DEFAULT, 'Animal', 'EpicKids Stuffed Penguin', '3 - 6 months', 8.50);
INSERT INTO MyToys.Product(id, category, name, ageRange, price) VALUES (DEFAULT, 'Animal', 'Aurora Foxie Fox Mini Flopsie 8" Stuffed Animal Plush', '3 years and up', 6.58);

INSERT INTO MyToys.Product(id, category, name, ageRange, price) VALUES (DEFAULT, 'Car', 'BMW i8 Concept 6-volt Electric Ride-On Car', '3 - 8 years', 164.83);
INSERT INTO MyToys.Product(id, category, name, ageRange, price) VALUES (DEFAULT, 'Car', 'Power Wheels Fisher-Price Barbie Jammin'' Jeep Wrangler', '36 months - 6 years', 383.85);

INSERT INTO MyToys.Product(id, category, name, ageRange, price) VALUES (DEFAULT, 'Doll', 'Glitter Girls', '36 months - 12 years',  17.05);
INSERT INTO MyToys.Product(id, category, name, ageRange, price) VALUES (DEFAULT, 'Doll', 'Baby Alive Luv ''n Snuggle', '18 months and up',  12.61);
INSERT INTO MyToys.Product(id, category, name, ageRange, price) VALUES (DEFAULT, 'Doll', 'Cabbage Patch Kids 14" Kids', '36 months - 15 years',  34.11);
INSERT INTO MyToys.Product(id, category, name, ageRange, price) VALUES (DEFAULT, 'Doll', 'Barbie Fairytale Ballerina', '36 months - 10 years',  5.16);

INSERT INTO MyToys.Product(id, category, name, ageRange, price) VALUES (DEFAULT, 'Lego', 'LEGO Star Wars Jedi & Clone Troopers', '6 - 12 years',  12.77);
INSERT INTO MyToys.Product(id, category, name, ageRange, price) VALUES (DEFAULT, 'Lego', 'LEGO Ideas Old Fishing Store', '12 years and up', 127.99);

INSERT INTO MyToys.Product(id, category, name, ageRange, price) VALUES (DEFAULT, 'Toy Soldier', 'Deluxe Bag of Classic Toy Green Army Soldiers', '3 - 20 years', 4.43);
INSERT INTO MyToys.Product(id, category, name, ageRange, price) VALUES (DEFAULT, 'Toy Soldier', 'TimMee Toy Walker Bulldog TANK Playset- Olive Green 13pc', '5 years and up', 22.00);
INSERT INTO MyToys.Product(id, category, name, ageRange, price) VALUES (DEFAULT, 'Toy Soldier', 'Plastic Toy Soldiers Napoleonic British Infantry Battle of Waterloo Painted Set', '8 years and up', 11.90);
