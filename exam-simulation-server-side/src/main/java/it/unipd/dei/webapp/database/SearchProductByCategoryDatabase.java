/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Searches products by their category.
 * 
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @author Maria Maistro (maistro@dei.unipd.it) 
 * @version 1.00
 * @since 1.00
 */
public final class SearchProductByCategoryDatabase {

	/**
	 * The SQL statement to be executed
	 */
	private static final String STATEMENT = "SELECT id, category, name, ageRange, price FROM MyToys.Product WHERE category = ?::MyToys.Category";

	/**
	 * The connection to the database
	 */
	private final Connection con;

	/**
	 * The category of the product
	 */
	private final String category;

	/**
	 * Creates a new object for searching products by category.
	 * 
	 * @param con
	 *            the connection to the database.
	 * @param category
	 *            the category of the product
	 */
	public SearchProductByCategoryDatabase(final Connection con, final String category) {
		this.con = con;
		this.category = category;
	}

	/**
	 * Searches products by category.
	 * 
	 * @return a list of {@code Product} objects matching the category.
	 * 
	 * @throws SQLException
	 *             if any error occurs while searching for products.
	 */
	public List<Product> searchProductByCategory() throws SQLException {

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		final List<Product> products = new ArrayList<Product>();

		try {
			pstmt = con.prepareStatement(STATEMENT);
			pstmt.setString(1, category);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				products.add(new Product(rs.getInt("id"), rs
						.getString("category"), rs.getString("name"),
						 rs.getString("ageRange"),
						rs.getDouble("price")));
			}
		} finally {
			if (rs != null) {
				rs.close();
			}

			if (pstmt != null) {
				pstmt.close();
			}

			con.close();
		}

		return products;
	}
}
