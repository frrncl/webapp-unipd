/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.CreateProductDatabase;
import it.unipd.dei.webapp.resource.Product;
import it.unipd.dei.webapp.resource.Message;
import it.unipd.dei.webapp.resource.ResourceList;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Searches products by their category.
 * 
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @author Maria Maistro (maistro@dei.unipd.it) 
 * @version 1.00
 * @since 1.00
 */
public final class CreateProductServlet extends AbstractDatabaseServlet {

	/**
	 * Searches products by their category.
	 * 
	 * @param req
	 *            the HTTP request from the client.
	 * @param res
	 *            the HTTP response from the server.
	 * 
	 * @throws ServletException
	 *             if any error occurs while executing the servlet.
	 * @throws IOException
	 *             if any error occurs in the client/server communication.
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		// set the MIME media type of the response
		res.setContentType("application/json; charset=utf-8");

		// get a stream to write the response
		OutputStream out = res.getOutputStream();

		try {
			final Product product =  Product.fromJSON(req.getInputStream());


			// creates a new object for accessing the database and creating products
			Product p = new CreateProductDatabase(getDataSource().getConnection(), product)
					.createProduct();

			if ( p!= null) {
				p.toJSON(out);
			} else {
				Message m = new Message("Cannot create product: unexpected error.",
										"E100", null);

				res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

				m.toJSON(out);
			}
		} catch (Exception ex) {
				Message m = new Message("Cannot create product: unexpected error.",
						"E100", ex.getMessage());
				
				res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				
				m.toJSON(out);
		}

		// flush the output stream buffer
		out.flush();

		// close the output stream
		out.close();
	}

}


