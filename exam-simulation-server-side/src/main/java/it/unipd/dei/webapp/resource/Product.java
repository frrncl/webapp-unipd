/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.resource;


import com.fasterxml.jackson.core.*;

import java.io.*;

/**
 * Represents the data about an Product.
 * 
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @author Maria Maistro (maistro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Product extends Resource {

	/**
	 * The unique identifier of the product
	 */
	private final int id;

	/**
	 * The category of the product
	 */
	private final String category;

	/**
	 * The name of the product
	 */
	private final String name;

	/**
	 * The suggested age range of the product
	 */
	private final String ageRange;

	/**
	 * The price of the product
	 */
	private final double price;

	/**
	 * Creates a new product.
	 * 
	 * @param id
	 *            the unique identifier of the product
	 * @param category
	 *            the category of the product.
	 * @param name
	 *            the name of the product
	 * @param ageRange
	 *            the suggested age range of the product
	 * @param price
	 *            the price of the product
	 */
	public Product(final int id, final String category, final String name, final String ageRange, final double price) {
		this.id = id;
		this.category = category;
		this.name = name;
		this.ageRange = ageRange;
		this.price = price;
	}

	/**
	 * Returns the identifier of the product.
	 * @return the identifier of the product.
	 */
	public final int getIdentifier() {
		return id;
	}

	/**
	 * Returns the category of the product.
	 * @return the category of the product.
	 */
	public final String getCategory() {
		return category;
	}

	/**
	 * Returns the name of the product.
	 * @return the name of the product.
	 */
	public final String getName() {
		return name;
	}

	/**
	 * Returns the ageRange of the product.
	 * @return the ageRange of the product.
	 */
	public final String getAgeRange() {
		return ageRange;
	}

	/**
	 * Returns the price of the product.
	 * @return the price of the product.
	 */
	public final double getPrice() {
		return price;
	}

	@Override
	public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("product");

		jg.writeStartObject();

		jg.writeNumberField("id", id);

		jg.writeStringField("category", category);

		jg.writeStringField("name", name);

		jg.writeStringField("ageRange", ageRange);

		jg.writeNumberField("price", price);

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}

	/**
	 * Creates a {@code Product} from its JSON representation.
	 *
	 * @param in the input stream containing the JSON document.
	 *
	 * @return the {@code Product} created from the JSON representation.
	 *
	 * @throws IOException if something goes wrong while parsing.
	 */
	public static Product fromJSON(final InputStream in) throws IOException {

		// the fields read from JSON
		int id = -1;
		String category = null;
		String name = null;
		String ageRange = null;
		double price = -1;

		final JsonParser jp = JSON_FACTORY.createParser(in);

		// while we are not on the start of an element or the element is not
		// a token element, advance to the next element (if any)
		while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "product".equals(jp.getCurrentName()) == false) {

			// there are no more events
			if (jp.nextToken() == null) {
				throw new IOException("Unable to parse JSON: no Product object found.");
			}
		}

		while (jp.nextToken() != JsonToken.END_OBJECT) {

			if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

				switch (jp.getCurrentName()) {
					case "id":
						jp.nextToken();
						id = jp.getIntValue();
						break;
					case "category":
						jp.nextToken();
						category = jp.getText();
						break;
					case "name":
						jp.nextToken();
						name = jp.getText();
						break;
					case "ageRange":
						jp.nextToken();
						ageRange = jp.getText();
						break;
					case "price":
						jp.nextToken();
						price = jp.getDoubleValue();
						break;
				}
			}
		}

		return new Product(id, category, name, ageRange, price);
	}
}
