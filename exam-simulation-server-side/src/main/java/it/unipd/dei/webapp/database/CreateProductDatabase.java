/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Creates a new product into the database.
 * 
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @author Maria Maistro (maistro@dei.unipd.it) 
 * @version 1.00
 * @since 1.00
 */
public final class CreateProductDatabase {

	/**
	 * The SQL statement to be executed
	 */
	private static final String STATEMENT = "INSERT INTO MyToys.Product(id, category, name, ageRange, price) VALUES (DEFAULT, ?::MyToys.Category, ?, ?, ?) RETURNING *";
	
	/**
	 * The connection to the database
	 */
	private final Connection con;

	/**
	 * The product to create
	 */
	private final Product product;

	/**
	 * Creates a new object for creating products.
	 * 
	 * @param con
	 *            the connection to the database.
	 * @param product
	 *            the product to create.
	 */
	public CreateProductDatabase(final Connection con, final Product product) {
		this.con = con;
		this.product = product;
	}

	/**
	 * Creates a new product.
	 * 
	 * @return the just created product
	 * 
	 * @throws SQLException
	 *             if any error occurs while creating products.
	 */
	public Product createProduct() throws SQLException {

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the creation
		Product p = null;

		try {
			pstmt = con.prepareStatement(STATEMENT);
			pstmt.setString(1, product.getCategory());
			pstmt.setString(2, product.getName());
			pstmt.setString(3, product.getAgeRange());
			pstmt.setDouble(4, product.getPrice());

			rs = pstmt.executeQuery();

			while (rs.next()) {
				p = new Product(rs.getInt("id"), rs.getString("category"),
								rs.getString("name"), rs.getString("ageRange"),
								rs.getDouble("price"));
			}
		} finally {
			if (rs != null) {
				rs.close();
			}

			if (pstmt != null) {
				pstmt.close();
			}

			con.close();
		}

		return p;
	}
}
