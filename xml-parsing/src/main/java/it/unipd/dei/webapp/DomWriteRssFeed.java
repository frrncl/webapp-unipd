/*
 * Copyright 2019 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unipd.dei.webapp;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

/**
 * Writes an RSS feed by using DOM.
 *
 * @author Nicola Ferro  (ferro@dei.unipd.it)
 * @version 1.0
 * @since 1.0
 */
public class DomWriteRssFeed {

    /**
     * Writes an RSS feed by using DOM.
     *
     * @param args args[0] the file to be read.
     *
     * @throws Exception if any error occurs while reading the file.
     */
    public static void main(String[] args) throws Exception {

        // the title of the feed
        String feedTitle = "Grid@CLEF News";

        // the description of the feed
        String feedDescription = "Events and updates about the Grid@CLEF track.";

        // the link to the feed
        String feedLink = "http://ims.dei.unipd.it/gridclef/";

        // total number of items in the feed
        int numItem = 2;

        // array containing the title of the items
        String[] itemTitle = new String[numItem];
        itemTitle[0] = "Terrier Support for Grid@CLEF";
        itemTitle[1] = "Registration to Grid@CLEF 2009 opens";

        // array containing the description of the items
        String[] itemDescription = new String[numItem];
        itemDescription[0] = "The Terrier open source information retrieval system will support the CIRCO framework.";
        itemDescription[1] = "Registration for Grid@CLEF 2009 opens today.";

        // array containing the publication date of the items
        String[] itemPubDate = new String[numItem];
        itemPubDate[0] = "Wed, 11 Feb 2009 15:41:49 GMT";
        itemPubDate[1] = "Wed, 4 Feb 2009 00:00:00 GMT";

        // array containing the link to the items
        String[] itemLink = new String[numItem];
        itemLink[0] = "http://ir.dcs.gla.ac.uk/terrier/issues/browse/TR-9";
        itemLink[1] = "http://www.clef-campaign.org/";

        // obtain an instance of the factory for creating DOM parsers
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        // obtain an instance of a DOM parsers
        DocumentBuilder parser = factory.newDocumentBuilder();

        // create a new XML document
        Document d = parser.newDocument();

        // a generic element
        Element e = null;

        // the rss element
        Element rss = null;

        // the channel element
        Element channel = null;

        // an item element
        Element item = null;

        // create the root rss element and append it to the document
        rss = d.createElement("rss");
        rss.setAttribute("version", "2.0");
        d.appendChild(rss);

        // create the channel element and append it to the rss element
        channel = d.createElement("channel");
        rss.appendChild(channel);

        // create the title element and append it to the channel element
        e = d.createElement("title");
        e.setTextContent(feedTitle);
        channel.appendChild(e);

        // create the description element and append it to the channel element
        e = d.createElement("description");
        e.setTextContent(feedDescription);
        channel.appendChild(e);

        // create the link element and append it to the channel element
        e = d.createElement("link");
        e.setTextContent(feedLink);
        channel.appendChild(e);

        // iterate over the items
        for (int i = 0; i < numItem; i++) {

            // create the item element and appen it to the channel element
            item = d.createElement("item");
            channel.appendChild(item);

            // create the title element and append it to the item element
            e = d.createElement("title");
            e.setTextContent(itemTitle[i]);
            item.appendChild(e);

            // create the pubDate element and append it to the item element
            e = d.createElement("pubDate");
            e.setTextContent(itemPubDate[i]);
            item.appendChild(e);

            // create the description element and append it to the item element
            e = d.createElement("description");
            e.setTextContent(itemDescription[i]);
            item.appendChild(e);

            // create the guid element and append it to the item element
            e = d.createElement("guid");
            e.setAttribute("isPermaLink", "false");
            e.setTextContent(Long.toString((long) Integer.MAX_VALUE + itemTitle[i].hashCode()));
            item.appendChild(e);

            // create the link element and append it to the item element
            e = d.createElement("link");
            e.setTextContent(itemLink[i]);
            item.appendChild(e);

        }

        // obtain a factory and an instance of a transformer, i.e. a class concerned with serializing XML
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer t = tf.newTransformer();
        t.setOutputProperty(OutputKeys.INDENT, "yes");
        t.setOutputProperty(OutputKeys.METHOD, "xml");
        t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");


        // wrap the document within a DOM source to write it in a file
        DOMSource doms = new DOMSource(d);

        // output where to write
        StreamResult sr = new StreamResult(new File(args[0]));

        // transform, i.e. write, the DOM source to the output file
        t.transform(doms, sr);

        System.out.printf("RSS feed successfully written to file: %s%n", args[0]);

    }

}