/*
 * Copyright 2019 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unipd.dei.webapp;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * Reads an RSS feed by using DOM.
 *
 * @author Nicola Ferro  (ferro@dei.unipd.it)
 * @version 1.0
 * @since 1.0
 */
public class DomReadRssFeed {

    /**
     * Reads an RSS feed by using DOM.
     *
     * @param args args[0] the file to be read.
     *
     * @throws Exception if any error occurs while reading the file.
     */
    public static void main(String[] args) throws Exception {

        // the title of the feed
        String feedTitle = null;

        // the description of the feed
        String feedDescription = null;

        // the link to the feed
        String feedLink = null;

        // the item currently processed
        int currentItem = 0;

        // the total number of items to be read
        int numItem = 0;

        // array containing the title of the items
        String[] itemTitle = null;

        // array containing the description of the items
        String[] itemDescription = null;

        // array containing the publication date of the items
        String[] itemPubDate = null;

        // array containing the link to the items
        String[] itemLink = null;

        // start time of the processing
        long start;

        // end time of the processing
        long end;

        // obtain an instance of the factory for creating DOM parsers
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        // obtain an instance of a DOM parser
        DocumentBuilder parser = factory.newDocumentBuilder();

        // start parsing
        start = System.currentTimeMillis();

        // parse the input file
        Document d = parser.parse(new File(args[0]));

        // end parsing
        end = System.currentTimeMillis();

        System.out.printf("Total time for parsing the document: %,d millisecs.%n", end - start);

        // determine the number of items in the feed
        numItem = d.getElementsByTagName("item").getLength();
        System.out.printf("The RSS feed contains %d elements%n", numItem);

        // allocate arrays
        itemTitle = new String[numItem];
        itemDescription = new String[numItem];
        itemPubDate = new String[numItem];
        itemLink = new String[numItem];

        // list of nodes
        NodeList channelList = null;
        NodeList itemList = null;

        // a generic node
        Node n = null;

        // start processing the DOM tree
        start = System.currentTimeMillis();

        // get the channel element
        channelList = d.getElementsByTagName("channel");
        n = channelList.item(0);

        // get the child nodes of the channel element
        channelList = n.getChildNodes();

        // iterate over the child nodes of the channel element
        for (int i = 0; i < channelList.getLength(); i++) {
            n = channelList.item(i);

            if (n.getNodeType() == Node.ELEMENT_NODE) {

                if (n.getNodeName().equals("title")) {
                    // get the title of the feed
                    feedTitle = ((Element) n).getTextContent();

                } else if (n.getNodeName().equals("description")) {
                    // get description of the feed
                    feedDescription = ((Element) n).getTextContent();

                } else if (n.getNodeName().equals("link")) {
                    // get the link to the feed
                    feedLink = ((Element) n).getTextContent();

                } else if (n.getNodeName().equals("item")) {
                    // we found an item of the feed

                    // get the child nodes of the item element
                    itemList = n.getChildNodes();

                    // iterate over the child nodes of the item element
                    for (int j = 0; j < itemList.getLength(); j++) {

                        n = itemList.item(j);

                        if (n.getNodeType() == Node.ELEMENT_NODE) {

                            if (n.getNodeName().equals("title")) {
                                // get the title of the item
                                itemTitle[currentItem] = ((Element) n).getTextContent();

                            } else if (n.getNodeName().equals("description")) {
                                // get the description of the item
                                itemDescription[currentItem] = ((Element) n).getTextContent();

                            } else if (n.getNodeName().equals("link")) {
                                // get the link of the item
                                itemLink[currentItem] = ((Element) n).getTextContent();

                            } else if (n.getNodeName().equals("pubDate")) {
                                // get the publication date of the item
                                itemPubDate[currentItem] = ((Element) n).getTextContent();

                            } else {
                                // unknown node, write it
                                System.out.printf("Unknown node %s within an item element.%n", n.getNodeName());
                            }
                        }
                    }

                    // increment the item counter
                    currentItem++;

                } else {
                    // unknown node, write it
                    System.out.printf("Unknown node %s within a channel element.%n", n.getNodeName());
                }

            }

        }

        // end of DOM tree processing
        end = System.currentTimeMillis();

        System.out.printf("%n---------------------------------%n");

        System.out.printf("Feed title: %s%n", feedTitle);
        System.out.printf("Feed description: %s%n", feedDescription);
        System.out.printf("Link to the feed: %s%n", feedLink);
        System.out.printf("Items of the feed:%n");
        for (int i = 0; i < numItem; i++) {
            System.out.printf(" - item title: %s%n", itemTitle[i]);
            System.out.printf(" - item description: %s%n", itemDescription[i]);
            System.out.printf(" - link to the item: %s%n", itemLink[i]);
            System.out.printf(" - item publication date: %s%n%n", itemPubDate[i]);
        }

        System.out.printf("Total time for processing the DOM tree: %,d millisecs.%n", end - start);


    }

}