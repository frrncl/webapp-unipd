/*
 * Copyright 2018-2023 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.SearchEmployeeBySalaryDAO;
import it.unipd.dei.webapp.resource.Actions;
import it.unipd.dei.webapp.resource.Employee;
import it.unipd.dei.webapp.resource.LogContext;
import it.unipd.dei.webapp.resource.Message;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.message.StringFormattedMessage;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

/**
 * Searches employees by their salary.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class SearchEmployeeBySalaryServlet extends AbstractDatabaseServlet {

	/**
	 * Searches employees by their salary.
	 *
	 * @param req the HTTP request from the client.
	 * @param res the HTTP response from the server.
	 *
	 * @throws IOException if any error occurs in the client/server communication.
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {

		LogContext.setIPAddress(req.getRemoteAddr());
		LogContext.setAction(Actions.SEARCH_EMPLOYEE_BY_SALARY);


		// request parameter
		int salary = -1;

		// model
		List<Employee> el = null;
		Message m = null;

		try {

			// retrieves the request parameter
			salary = Integer.parseInt(req.getParameter("salary"));

			// creates a new object for accessing the database and searching the employees
			el = new SearchEmployeeBySalaryDAO(getConnection(), salary).access().getOutputParam();

			m = new Message("Employees successfully searched.");

			LOGGER.info("Employees successfully searched by salary %d.", salary);

		} catch (NumberFormatException ex) {
			m = new Message("Cannot search for employees. Invalid input parameters: salary must be integer.", "E100",
					ex.getMessage());

			LOGGER.error("Cannot search for employees. Invalid input parameters: salary must be integer.", ex);
		} catch (SQLException ex) {
			m = new Message("Cannot search for employees: unexpected error while accessing the database.", "E200",
					ex.getMessage());

			LOGGER.error("Cannot search for employees: unexpected error while accessing the database.", ex);
		}

		try {
			// set the MIME media type of the response
			res.setContentType("text/html; charset=utf-8");

			// get a stream to write the response
			PrintWriter out = res.getWriter();

			// write the HTML page
			out.printf("<!DOCTYPE html>%n");

			out.printf("<html lang=\"en\">%n");
			out.printf("<head>%n");
			out.printf("<meta charset=\"utf-8\">%n");
			out.printf("<title>Search Employee</title>%n");
			out.printf("</head>%n");

			out.printf("<body>%n");
			out.printf("<h1>Search Employee</h1>%n");
			out.printf("<hr/>%n");

			if (m.isError()) {
				out.printf("<ul>%n");
				out.printf("<li>error code: %s</li>%n", m.getErrorCode());
				out.printf("<li>message: %s</li>%n", m.getMessage());
				out.printf("<li>details: %s</li>%n", m.getErrorDetails());
				out.printf("</ul>%n");
			} else {
				out.printf("<p>%s</p>%n", m.getMessage());

				out.printf("<table>%n");
				out.printf("<tr>%n");
				out.printf("<td>Badge</td><td>Surname</td><td>Age</td><td>Salary</td>%n");
				out.printf("</tr>%n");

				for (Employee e : el) {
					out.printf("<tr>%n");
					out.printf("<td>%s</td><td>%s</td><td>%s</td><td>%s</td>%n", e.getBadge(), e.getSurname(),
							e.getAge(), e.getSalary());
					out.printf("</tr>%n");
				}
				out.printf("</table>%n");
			}

			out.printf("</body>%n");

			out.printf("</html>%n");

			// flush the output stream buffer
			out.flush();

			// close the output stream
			out.close();
		} catch (IOException ex) {
			LOGGER.error(new StringFormattedMessage("Unable to send response when creating employee %d.", salary), ex);
			throw ex;
		} finally {
			LogContext.removeIPAddress();
			LogContext.removeAction();
			LogContext.removeUser();
		}
	}

}
