/*
 * Copyright 2018-2023 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Employee;
import org.apache.logging.log4j.message.StringFormattedMessage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Creates a new employee into the database.
 * 
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class CreateEmployeeDAO extends AbstractDAO {

	/**
	 * The SQL statement to be executed
	 */
	private static final String STATEMENT = "INSERT INTO Ferro.Employee (badge, surname, age, salary) VALUES (?, ?, ?, ?)";

	/**
	/**
	 * The employee to be stored into the database
	 */
	private final Employee employee;

	/**
	 * Creates a new object for storing an employee into the database.
	 * 
	 * @param con
	 *            the connection to the database.
	 * @param employee
	 *            the employee to be stored into the database.
	 */
	public CreateEmployeeDAO(final Connection con, final Employee employee) {
		super(con);

		if (employee == null) {
			LOGGER.error("The employee cannot be null.");
			throw new NullPointerException("The employee cannot be null.");
		}

		this.employee = employee;
	}

	@Override
	protected final void doAccess() throws SQLException {

		PreparedStatement pstmt = null;

		try {
			pstmt = con.prepareStatement(STATEMENT);
			pstmt.setInt(1, employee.getBadge());
			pstmt.setString(2, employee.getSurname());
			pstmt.setInt(3, employee.getAge());
			pstmt.setInt(4, employee.getSalary());

			pstmt.execute();

			LOGGER.info("Employee %d successfully stored in the database.", employee.getBadge());
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
		}

	}
}
