/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.CreateEmployeeDAO;
import it.unipd.dei.webapp.resource.Actions;
import it.unipd.dei.webapp.resource.Employee;
import it.unipd.dei.webapp.resource.LogContext;
import it.unipd.dei.webapp.resource.Message;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.message.StringFormattedMessage;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

/**
 * Creates a new employee into the database.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class CreateEmployeeServlet extends AbstractDatabaseServlet {

	/**
	 * Creates a new employee into the database.
	 *
	 * @param req the HTTP request from the client.
	 * @param res the HTTP response from the server.
	 *
	 * @throws IOException if any error occurs in the client/server communication.
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {

		LogContext.setIPAddress(req.getRemoteAddr());
		LogContext.setAction(Actions.CREATE_EMPLOYEE);

		// request parameters
		int badge = -1;
		String surname = null;
		int age = -1;
		int salary = -1;

		// model
		Employee e = null;
		Message m = null;

		try {
			// retrieves the request parameters
			badge = Integer.parseInt(req.getParameter("badge"));
			surname = req.getParameter("surname");
			age = Integer.parseInt(req.getParameter("age"));
			salary = Integer.parseInt(req.getParameter("salary"));

			// set the badge of the employee as the resource in the log context
			// at this point we know it is a valid integer
			LogContext.setResource(req.getParameter("badge"));

			// creates a new employee from the request parameters
			e = new Employee(badge, surname, age, salary);

			// creates a new object for accessing the database and stores the employee
			new CreateEmployeeDAO(getConnection(), e).access();

			m = new Message(String.format("Employee %d successfully created.", badge));

			LOGGER.info("Employee %d successfully created in the database.", badge);

		} catch (NumberFormatException ex) {
			m = new Message(
					"Cannot create the employee. Invalid input parameters: badge, age, and salary must be integer.",
					"E100", ex.getMessage());

			LOGGER.error(
					"Cannot create the employee. Invalid input parameters: badge, age, and salary must be integer.",
					ex);
		} catch (SQLException ex) {
			if ("23505".equals(ex.getSQLState())) {
				m = new Message(String.format("Cannot create the employee: employee %d already exists.", badge), "E300",
						ex.getMessage());

				LOGGER.error(
						new StringFormattedMessage("Cannot create the employee: employee %d already exists.", badge),
						ex);
			} else {
				m = new Message("Cannot create the employee: unexpected error while accessing the database.", "E200",
						ex.getMessage());

				LOGGER.error("Cannot create the employee: unexpected error while accessing the database.", ex);
			}
		}

		try {
			// set the MIME media type of the response
			res.setContentType("text/html; charset=utf-8");

			// get a stream to write the response
			PrintWriter out = res.getWriter();

			// write the HTML page
			out.printf("<!DOCTYPE html>%n");

			out.printf("<html lang=\"en\">%n");
			out.printf("<head>%n");
			out.printf("<meta charset=\"utf-8\">%n");
			out.printf("<title>Create Employee</title>%n");
			out.printf("</head>%n");

			out.printf("<body>%n");
			out.printf("<h1>Create Employee</h1>%n");
			out.printf("<hr/>%n");

			if (m.isError()) {
				out.printf("<ul>%n");
				out.printf("<li>error code: %s</li>%n", m.getErrorCode());
				out.printf("<li>message: %s</li>%n", m.getMessage());
				out.printf("<li>details: %s</li>%n", m.getErrorDetails());
				out.printf("</ul>%n");
			} else {
				out.printf("<p>%s</p>%n", m.getMessage());
				out.printf("<ul>%n");
				out.printf("<li>badge: %s</li>%n", e.getBadge());
				out.printf("<li>surname: %s</li>%n", e.getSurname());
				out.printf("<li>age: %s</li>%n", e.getAge());
				out.printf("<li>salary: %s</li>%n", e.getSalary());
				out.printf("</ul>%n");
			}

			out.printf("</body>%n");

			out.printf("</html>%n");

			// flush the output stream buffer
			out.flush();

			// close the output stream
			out.close();
		} catch (IOException ex) {
			LOGGER.error(new StringFormattedMessage("Unable to send response when creating employee %d.", badge), ex);
			throw ex;
		} finally {
			LogContext.removeIPAddress();
			LogContext.removeAction();
			LogContext.removeResource();
		}

	}

}
