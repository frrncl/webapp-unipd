/*
 * Copyright 2018-2023 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Authenticates the user in the database
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class AuthenticateUserDAO extends AbstractDAO<Boolean> {

	/**
	 * The SQL statement to be executed
	 */
	private static final String STATEMENT = "...";

	/**
	 * The username of the user to be authenticated
	 */
	private final String username;

	/**
	 * The password of the user to be authenticated
	 */

	private final String password;

	/**
	 * Creates a new object for storing an employee into the database.
	 *
	 * @param con      the connection to the database.
	 * @param username the username of the user to be authenticated.
	 * @param password the password of the user to be authenticated.
	 */
	public AuthenticateUserDAO(final Connection con, final String username, final String password) {
		super(con);

		if (username == null || username.isBlank()) {
			LOGGER.error("The username cannot be null or empty.");
			throw new NullPointerException("The username cannot be null or empty.");
		}

		this.username = username;

		if (password == null || password.isBlank()) {
			LOGGER.error("The password cannot be null or empty.");
			throw new NullPointerException("The password cannot be null or empty.");
		}

		this.password = password;

	}

	@Override
	protected final void doAccess() throws SQLException {

		 /*
            Here there should be some logic to actually authenticate the user within the database,
            by using the provided username and password. Typically, something like
            SELECT *
            	FROM User
            	WHERE username = ? AND password = ?

            Not implemented for simplicity. Therefore, all the users will be authenticated and
            the outputParam is set to true ;-)
          */
		outputParam = true;

		LOGGER.info("User %s successfully authenticated", username);
	}
}
