<!--
Copyright 2020-2023 University of Padua, Italy

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Author: Nicola Ferro (ferro@dei.unipd.it)
Version: 1.0
Since: 1.0
-->

<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<!-- even if it should not happen, better to check to have a user in session and,
    if not there, take appropriate action-->
<c:choose>
    <c:when test="${empty sessionScope.user}">

        <head>
            <meta charset="utf-8">
            <title>Unauthorized access - You shouldn't be here!</title>
        </head>

        <body>
        <h1>UNAUTHORIZED ACCESS</h1>

        <p>You shouldn't be here, UNKNOWN USER!!!!! </p>

        </body>
    </c:when>

    <c:otherwise>

        <head>
            <meta charset="utf-8">
            <title>Create Employee Form</title>
        </head>

        <body>
        <h1>Create Employee Form</h1>

        <p>
            Welcome back, <c:out value="${sessionScope.user}"/>!
        </p>

        <form method="POST" action="<c:url value="/protected/create-employee"/>">
            <label for="badgeID">Badge:</label>
            <input id="badgeID" name="badge" type="text"/><br/>

            <label for="surnameID">Surname:</label>
            <input id="surnameID" name="surname" type="text"/><br/>

            <label for="ageID">Age:</label>
            <input id="ageID" name="age" type="text"/><br/>

            <label for="salaryID">Salary:</label>
            <input id="salaryID" name="salary" type="text"/><br/><br/>

            <button type="submit">Submit</button>
            <br/>
            <button type="reset">Reset the form</button>
        </form>
        </body>
    </c:otherwise>
</c:choose>

</html>
