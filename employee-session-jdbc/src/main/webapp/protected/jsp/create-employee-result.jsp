<!--
Copyright 2020-2023 University of Padua, Italy

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Author: Nicola Ferro (ferro@dei.unipd.it)
Version: 1.0
Since: 1.0
-->

<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<!-- even if it should not happen, better to check to have a user in session and,
if not there, take appropriate action-->
<c:choose>
    <c:when test="${empty sessionScope.user}">

        <head>
            <meta charset="utf-8">
            <title>Unauthorized access - You shouldn't be here!</title>
        </head>

        <body>
        <h1>UNAUTHORIZED ACCESS</h1>

        <p>You shouldn't be here, UNKNOWN USER!!!!! </p>

        </body>
    </c:when>

    <c:otherwise>


        <head>
            <title>Create Employee</title>
        </head>

        <body>
        <h1>Create Employee</h1>
        <p>
            Welcome back, <c:out value="${sessionScope.user}"/>!
        </p>
        <hr/>

        <!-- display the message -->
        <c:import url="/jsp/include/show-message.jsp"/>

        <!-- display the just created employee, if any and no errors -->
        <c:if test='${not empty employee && !message.error}'>
            <ul>
                <li>badge: <c:out value="${employee.badge}"/></li>
                <li>surname: <c:out value="${employee.surname}"/></li>
                <li>age: <c:out value="${employee.age}"/></li>
                <li>salary: <c:out value="${employee.salary}"/></li>
            </ul>
        </c:if>
        </body>
    </c:otherwise>
</c:choose>


</html>
