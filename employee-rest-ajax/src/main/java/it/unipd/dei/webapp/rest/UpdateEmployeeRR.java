/*
 * Copyright 2023 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.rest;

import it.unipd.dei.webapp.database.UpdateEmployeeDAO;
import it.unipd.dei.webapp.resource.Actions;
import it.unipd.dei.webapp.resource.Employee;
import it.unipd.dei.webapp.resource.LogContext;
import it.unipd.dei.webapp.resource.Message;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.EOFException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * A REST resource for updating {@link Employee}s.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class UpdateEmployeeRR extends AbstractRR {

	/**
	 * Creates a new REST resource for updating {@code Employee}s.
	 *
	 * @param req the HTTP request.
	 * @param res the HTTP response.
	 * @param con the connection to the database.
	 */
	public UpdateEmployeeRR(final HttpServletRequest req, final HttpServletResponse res, Connection con) {
		super(Actions.UPDATE_EMPLOYEE, req, res, con);
	}


	@Override
	protected void doServe() throws IOException {

		Employee e = null;
		Message m = null;

		try {
			// parse the URI path to extract the badge
			String path = req.getRequestURI();
			path = path.substring(path.lastIndexOf("employee") + 8);

			final int badge = Integer.parseInt(path.substring(1));

			LogContext.setResource(Integer.toString(badge));

			final Employee employee = Employee.fromJSON(req.getInputStream());

			if (badge != employee.getBadge()) {
				LOGGER.warn("Cannot update the employee: URI request (%d) and employee resource (%d) badges differ.",
						badge, employee.getBadge());

				m = new Message("Cannot update the employee: URI request and employee resource badges differ.", "E4A8",
						String.format("Request URI badge %d; employee resource badge %d.", badge, employee.getBadge()));
				res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				m.toJSON(res.getOutputStream());
				return;
			}


			// creates a new DAO for accessing the database and updates the employee
			e = new UpdateEmployeeDAO(con, employee).access().getOutputParam();

			if (e != null) {
				LOGGER.info("Employee successfully updated.");

				res.setStatus(HttpServletResponse.SC_OK);
				e.toJSON(res.getOutputStream());
			} else {
				LOGGER.warn("Employee not found. Cannot update it.");

				m = new Message(String.format("Employee %d not found. Cannot update it.", badge), "E5A3", null);
				res.setStatus(HttpServletResponse.SC_NOT_FOUND);
				m.toJSON(res.getOutputStream());
			}
		} catch (IndexOutOfBoundsException | NumberFormatException ex) {
			LOGGER.warn("Cannot delete the employee: wrong format for URI /employee/{badge}.", ex);

			m = new Message("Cannot delete the employee: wrong format for URI /employee/{badge}.", "E4A7",
					ex.getMessage());
			res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			m.toJSON(res.getOutputStream());
		} catch (EOFException ex) {
			LOGGER.warn("Cannot updated the employee: no Employee JSON object found in the request.", ex);

			m = new Message("Cannot update the employee: no Employee JSON object found in the request.", "E4A8",
					ex.getMessage());
			res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			m.toJSON(res.getOutputStream());
		} catch (SQLException ex) {
			if ("23503".equals(ex.getSQLState())) {
				LOGGER.warn("Cannot delete the employee: other resources depend on it.");

				m = new Message("Cannot delete the employee: other resources depend on it.", "E5A4", ex.getMessage());
				res.setStatus(HttpServletResponse.SC_CONFLICT);
				m.toJSON(res.getOutputStream());
			} else {
				LOGGER.error("Cannot delete the employee: unexpected database error.", ex);

				m = new Message("Cannot delete the employee: unexpected database error.", "E5A1", ex.getMessage());
				res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				m.toJSON(res.getOutputStream());
			}
		}
	}

}
