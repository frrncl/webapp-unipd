/*
 * Copyright 2023 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.rest;

import it.unipd.dei.webapp.database.ListEmployeeDAO;
import it.unipd.dei.webapp.database.SearchEmployeeBySalaryDAO;
import it.unipd.dei.webapp.resource.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * A REST resource for searching {@link Employee}s by salary.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class SearchEmployeeBySalaryRR extends AbstractRR {

	/**
	 * Creates a new REST resource for searching {@code Employee}s by salary.
	 *
	 * @param req the HTTP request.
	 * @param res the HTTP response.
	 * @param con the connection to the database.
	 */
	public SearchEmployeeBySalaryRR(final HttpServletRequest req, final HttpServletResponse res, Connection con) {
		super(Actions.SEARCH_EMPLOYEE_BY_SALARY, req, res, con);
	}


	@Override
	protected void doServe() throws IOException {

		List<Employee> el = null;
		Message m = null;

		try {
			// parse the URI path to extract the salary
			String path = req.getRequestURI();
			path = path.substring(path.lastIndexOf("salary") + 6);

			final int salary = Integer.parseInt(path.substring(1));

			LogContext.setResource(Integer.toString(salary));

			// creates a new DAO for accessing the database and searches the employee(s)
			el = new SearchEmployeeBySalaryDAO(con, salary).access().getOutputParam();

			if (el != null) {
				LOGGER.info("Employee(s) successfully searched by salary %d.", salary);

				res.setStatus(HttpServletResponse.SC_OK);
				new ResourceList(el).toJSON(res.getOutputStream());
			} else { // it should not happen
				LOGGER.error("Fatal error while searching employee(s).");

				m = new Message("Cannot search employee(s): unexpected error.", "E5A1", null);
				res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				m.toJSON(res.getOutputStream());
			}
		} catch (IndexOutOfBoundsException | NumberFormatException ex) {
			LOGGER.warn("Cannot search employee(s): wrong format for URI /employee/salary/{salary}.", ex);

			m = new Message("Cannot search employee(s): wrong format for URI /employee/salary/{salary}.", "E4A7",
					ex.getMessage());
			res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			m.toJSON(res.getOutputStream());
		} catch (SQLException ex) {
			LOGGER.error("Cannot search employee(s): unexpected database error.", ex);

			m = new Message("Cannot search employee(s): unexpected database error.", "E5A1", ex.getMessage());
			res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			m.toJSON(res.getOutputStream());
		}
	}


}
