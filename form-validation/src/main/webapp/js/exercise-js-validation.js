/*
 Copyright 2018 University of Padua, Italy

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Maria Maistro (maistro@dei.unipd.it)
 Version: 1.0
 Since: 1.0
*/


// Get the form element
var form = document.getElementsByTagName("form")[0];

// Get the email input element
var email = document.getElementById("provide_email");

// Get the error span
var error = email.nextElementSibling;

// Regular expression to check whether the input is an email address
var emailRegExp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

// This defines what happens when the user insert the email text
email.addEventListener("input", function () {
    var test = email.value.length > 0 && emailRegExp.test(email.value);

    if (test) {
        email.className = "valid";
        error.innerHTML = "";
    } else {
        email.className = "invalid";
        error.innerHTML = "Please insert an e-mail address! [oninput check]";
        error.className = "error";
    }
});


// This defines what happens when the user tries to submit the data
form.addEventListener("submit", function (event) {
    var test = email.value.length > 0 && emailRegExp.test(email.value);
    if (test) {
        email.className = "valid";
        error.innerHTML = "";
        error.className = "error";
    } else {
        email.className = "invalid";
        error.innerHTML = "I expect an e-mail! [onsubmit check]";
        error.className = "error active";
        event.preventDefault();
    }
});
