<!--
 Copyright 2018 University of Padua, Italy
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 
 Author: Nicola Ferro (ferro@dei.unipd.it)
 		 Maria Maistro (maistro@dei.unipd.it)
 Version: 1.0
 Since: 1.0
-->

<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="example web application for exam simulation">
	<meta name="author" content="Nicola Ferro">
	<meta name="author" content="Maria Maistro">

	<title>MyToys</title>

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

	<!-- Font Awesome CSS-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

	<!-- Custom styles for this template -->
	<link href="<c:url value="/css/style.css"/>" rel="stylesheet">
  </head>

  <body>
	
	<div class="container containerOver">

	<header>
		<div class="row">
			<div class="col-md-10 text-center">
				<h1 id="page_title">MyToys Catalog</h1>
			</div>

			<div class="col-md-2">
				<figure class="figure">
					<img src="<c:url value="/figure/mytoys_logo.png"/>" class="figure-img img-fluid rounded" alt="Logo of the MyToys company." id="mytoys_logo">
				</figure>
			</div>
		</div>
	</header>

	<div class="content">

		<nav class="nav nav-tabs" id="myTab" role="tablist">
			<a class="nav-item nav-link " id="nav-create-tab" data-toggle="tab" href="#nav-create" role="tab" aria-controls="nav-create" aria-selected="false">Create</a>
			<a class="nav-item nav-link" id="nav-browse-tab" data-toggle="tab" href="#nav-browse" role="tab" aria-controls="nav-browse" aria-selected="false">Browse</a>
			<a class="nav-item nav-link active" id="nav-search-tab" data-toggle="tab" href="#nav-search" role="tab" aria-controls="nav-search" aria-selected="true">Search</a>
		</nav>


		<div class="tab-content" id="nav-tabContent">
			<div class="tab-pane fade addMargin" id="nav-create" role="tabpanel" aria-labelledby="nav-create-tab">
				Still under construction.
			</div>
			<div class="tab-pane fade addMargin" id="nav-browse" role="tabpanel" aria-labelledby="nav-browse-tab">
					Still under construction.
			</div>
			<div class="tab-pane fade show active addMargin" id="nav-search" role="tabpanel" aria-labelledby="nav-search-tab">
				
				<form id="searchByCategory" method="GET" action="http://dbstud.dei.unipd.it:8080/mytoys-server/search-product-by-category">
					<div class="row">
						<div class="col-md-12">
							<label for="searchByCategory">Search by Category</label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							 <select class="form-control form-control-lg" name="category" id="categoryValue">
								<option value="Animal">Animal</option>
								<option value="Car">Car</option>
								<option value="Doll">Doll</option>
								<option value="Lego">Lego</option>
								<option value="Toy Soldier">Toy Soldier</option>
							</select>
						</div>
						<div class="col-md-6">
							 <button type="submit" id="button_search" class="btn btn-lg">
							 	<i class="fas fa-search"></i> Search
							 </button>
						</div>
					</div>
					
					<div class="row addMargin">
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<hr/>
						</div>
						<div class="col-md-2"></div>
					</div>
					
					<div class="row text-center">
						<div class="col-md-1"></div>
						<div class="col-md-10">
							<div id="results"></div>
						</div>
						<div class="col-md-1"></div>
					</div>
				</form>
				
				
			</div>
		</div>	  
	  
	</div> <!-- End of Content-->

	</div><!-- /.container -->

	<!-- Bootstrap, Popper, and JQuery JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	
	<script src="<c:url value="/js/mytoys.js"/>"></script>
  </body>
</html>