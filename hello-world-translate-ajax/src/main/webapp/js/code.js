
/*
 Copyright 2019 University of Padua, Italy

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Nicola Ferro (ferro@dei.unipd.it)
 Version: 1.0
 Since: 1.0
*/

// register an event handler for the submit event of the form
document.getElementById('translateForm').addEventListener("submit", callYandexTranslate);
console.log("Handler for onsubmit events of the translateForm form successfully registered.");


// this function performs the call to the Yandex Translate API
function callYandexTranslate(event) {

	// retrieve the Yandex API key from the form
	const apiKey =  document.getElementById('yandexApiKey').value;

	// retrieve the target language from the form
	const targetLanguage =  document.getElementById('targetLanguage').value;

	// the URL of the Yandex Translate API
	// remember to URL encode when needed
	const url = `https://translate.yandex.net/api/v1.5/tr.json/translate?key=${apiKey}&text=Hello%20World&lang=en-${targetLanguage}`;

	// The XMLHttpRequest object for making the AJAX call
	const xhr = new XMLHttpRequest();

	if (!xhr) {
		console.error("Unable to instantiate the XMLHttpRequest object.");

		// get the div where to write results
		const div = document.getElementById('results');

		// generic DOM elements
		let e, ee, eee;

		/*
			Create the following fragment of HTML

			<div class="alert alert-danger alert-dismissible fade show" role="alert">
				Unable to instantiate the XMLHttpRequest object.
				<button type="button" class="close" data-dismiss="alert">
					<i class="fas fa-times"></i>
				</button>
			</div>
		 */
		e = document.createElement('div');
		div.appendChild(e);

		e.className = "alert alert-danger alert-dismissible fade show";
		e.setAttribute("role", "alert");
		e.appendChild(document.createTextNode("Unable to instantiate the XMLHttpRequest object."));

		ee = document.createElement('button');
		e.appendChild(ee);

		ee.type = "button";
		ee.className = "close";
		ee.setAttribute("data-dismiss", "alert");

		eee = document.createElement('i');
		ee.appendChild(eee);
		eee.className = "fas fa-times";

		return false;
	}

	// set the handler of the results;
	xhr.onload = function () {
        processYandexTranslateResults(xhr);
    };

	// perform the AJAX request
	xhr.open("GET", url);
	xhr.send();

	console.log("AJAX request performed.")

	// stop the form from submitting the normal way and refreshing the page
	event.preventDefault();
}

// this function process the results of the call to the Yandex Translate API
function processYandexTranslateResults(xhr) {

    /*
       Create the following fragment of HTML. It changes the type of alert (danger or success)
       and the message, depending on the cases.

       <div class="alert alert-XXX alert-dismissible fade show" role="alert">
           Message.
           <button type="button" class="close" data-dismiss="alert">
               <i class="fas fa-times"></i>
           </button>
       </div>
    */


    // get the div where to write results
    const div = document.getElementById('results');

    // generic DOM elements
    const e = document.createElement('div');
    e.setAttribute("role", "alert");
    div.appendChild(e);

    let ee, eee;

    if (xhr.readyState === XMLHttpRequest.DONE) {

        // parse the response to the corresponding JSON object
        let jsonData = JSON.parse(xhr.responseText);

        console.log(xhr.status);
        console.log(xhr.responseText);
        
        switch (jsonData["code"]) {

            case 200:
                e.className = "alert alert-success alert-dismissible fade show";
                e.appendChild(document.createTextNode(jsonData["text"][0]));

                console.info("Translation successfully performed.");
                break;

            default:
                e.className = "alert alert-danger alert-dismissible fade show";
                e.appendChild(document.createTextNode(jsonData["message"]));

                console.error("Translation not performed. " + jsonData["message"]);

                break;
        }

    } else {
        e.className = "alert alert-danger alert-dismissible fade show";
        e.appendChild(document.createTextNode("Unexpected error."));

        console.error("Unexpected error while performing the AJAX request.");
    }

    ee = document.createElement('button');
    e.appendChild(ee);

    ee.type = "button";
    ee.className = "close";
    ee.setAttribute("data-dismiss", "alert");

    eee = document.createElement('i');
    ee.appendChild(eee);
    eee.className = "fas fa-times";

}
