/*
 * Copyright 2023 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Employee;

import javax.naming.directory.NoSuchAttributeException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.MissingResourceException;

/**
 * Loads the photo of an Employee.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class LoadEmployeePhotoDAO extends AbstractDAO<Employee> {

	/**
	 * The SQL statement to be executed
	 */
	private static final String STATEMENT = "SELECT photo, photoMediaType FROM Ferro.Employee WHERE badge = ?";

	/**
	 * The badge of the employee
	 */
	private final int badge;

	/**
	 * Creates a new object for loading employee photos.
	 *
	 * @param con   the connection to the database.
	 * @param badge the badge of the employee.
	 */
	public LoadEmployeePhotoDAO(final Connection con, final int badge) {
		super(con);
		this.badge = badge;
	}

	@Override
	public final void doAccess() throws SQLException {

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		Employee e = null;

		try {
			pstmt = con.prepareStatement(STATEMENT);
			pstmt.setInt(1, badge);

			rs = pstmt.executeQuery();

			if (rs.next()) {
				e = new Employee(Integer.MIN_VALUE, null, Integer.MIN_VALUE, Integer.MIN_VALUE, null,
						rs.getBytes("photo"), rs.getString("photoMediaType"));

				LOGGER.info("Photo for employee %d successfully loaded.", badge);
			} else {
				LOGGER.warn("Employee %d not found.", badge);
				throw new SQLException(String.format("Employee %d not found.", badge), "NOT_FOUND");
			}


		} finally {
			if (rs != null) {
				rs.close();
			}

			if (pstmt != null) {
				pstmt.close();
			}

		}

		this.outputParam = e;
	}
}
