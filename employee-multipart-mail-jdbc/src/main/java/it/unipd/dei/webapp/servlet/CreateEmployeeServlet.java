/*
 * Copyright 2018-2023 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.CreateEmployeeDAO;
import it.unipd.dei.webapp.mail.MailManager;
import it.unipd.dei.webapp.resource.Actions;
import it.unipd.dei.webapp.resource.Employee;
import it.unipd.dei.webapp.resource.LogContext;
import it.unipd.dei.webapp.resource.Message;
import jakarta.mail.MessagingException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import org.apache.logging.log4j.message.StringFormattedMessage;

import java.awt.datatransfer.MimeTypeParseException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;

/**
 * Creates a new employee into the database.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class CreateEmployeeServlet extends AbstractDatabaseServlet {

	/**
	 * Creates a new employee into the database.
	 *
	 * @param req the HTTP request from the client.
	 * @param res the HTTP response from the server.
	 *
	 * @throws ServletException if any error occurs while executing the servlet.
	 * @throws IOException      if any error occurs in the client/server communication.
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		LogContext.setIPAddress(req.getRemoteAddr());
		LogContext.setAction(Actions.CREATE_EMPLOYEE);

		// model
		Employee e = null;
		Message m = null;

		try {
			e = parseRequest(req);

			LogContext.setResource(Integer.toString(e.getBadge()));

			// creates a new object for accessing the database and stores the employee
			new CreateEmployeeDAO(getConnection(), e).access();

			LOGGER.info("Employee %d successfully created in the database.", e.getBadge());

			sendCreationConfirmationEmail(e);

			LOGGER.info("Creation confirmation email for employee %d successfully sent.", e.getBadge());

			m = new Message(String.format("Employee %d successfully created and confirmation email successfully sent.",
					e.getBadge()));

		} catch (NumberFormatException ex) {
			m = new Message(
					"Cannot create the employee. Invalid input parameters: badge, age, and salary must be integer.",
					"E100", ex.getMessage());

			LOGGER.error(
					"Cannot create the employee. Invalid input parameters: badge, age, and salary must be integer.",
					ex);
		} catch (SQLException ex) {
			if ("23505".equals(ex.getSQLState())) {
				m = new Message(String.format("Cannot create the employee: employee %d already exists.", e.getBadge()),
						"E300", ex.getMessage());

				LOGGER.error(new StringFormattedMessage("Cannot create the employee: employee %d already exists.",
						e.getBadge()), ex);
			} else {
				m = new Message("Cannot create the employee: unexpected error while accessing the database.", "E200",
						ex.getMessage());

				LOGGER.error("Cannot create the employee: unexpected error while accessing the database.", ex);
			}
		} catch (MimeTypeParseException ex) {
			m = new Message(
					String.format("Unsupported MIME media type for employee photo. Expected: image/png or image/jpeg."),
					"E400", ex.getMessage());
		} catch (MessagingException ex) {
			m = new Message(String.format("Employee %d successfully created but unable to send confirmation email.",
					e.getBadge()));

			LOGGER.warn(new StringFormattedMessage(
					"Employee %d successfully created but unable to send confirmation email.", e.getBadge()), ex);
		}

		try {
			// stores the employee and the message as a request attribute
			req.setAttribute("employee", e);
			req.setAttribute("message", m);

			// forwards the control to the create-employee-result JSP
			req.getRequestDispatcher("/jsp/create-employee-result.jsp").forward(req, res);
		} catch (Exception ex) {
			LOGGER.error(
					new StringFormattedMessage("Unable to send response after successfuly creation of employee %d.",
							e.getBadge()), ex);
			throw ex;
		} finally {
			LogContext.removeIPAddress();
			LogContext.removeAction();
			LogContext.removeResource();
		}

	}

	/**
	 * Parses the HTTP request and returns a new {@code Employee} from it.
	 *
	 * @param req the HTTP request.
	 *
	 * @return the {@code Employee} object created from the HTTP request.
	 *
	 * @throws ServletException       if something goes wrong while parsing the request.
	 * @throws IOException            if something goes wrong while parsing the request.
	 * @throws MimeTypeParseException if something goes wrong while parsing the request.
	 */
	private Employee parseRequest(HttpServletRequest req) throws ServletException, IOException, MimeTypeParseException {

		// request parameters
		int badge = -1;
		String surname = null;
		int age = -1;
		int salary = -1;
		String email = null;
		byte[] photo = null;
		String photoMediaType = null;

		// retrieves the request parameters
		for (Part p : req.getParts()) {

			switch (p.getName()) {
				case "badge":

					try (InputStream is = p.getInputStream()) {
						badge = Integer.parseInt(new String(is.readAllBytes(), StandardCharsets.UTF_8).trim());
					}
					break;

				case "surname":
					try (InputStream is = p.getInputStream()) {
						surname = new String(is.readAllBytes(), StandardCharsets.UTF_8).trim();
					}
					break;

				case "age":
					try (InputStream is = p.getInputStream()) {
						age = Integer.parseInt(new String(is.readAllBytes(), StandardCharsets.UTF_8).trim());
					}
					break;

				case "salary":
					try (InputStream is = p.getInputStream()) {
						salary = Integer.parseInt(new String(is.readAllBytes(), StandardCharsets.UTF_8).trim());
					}
					break;

				case "email":
					try (InputStream is = p.getInputStream()) {
						email = new String(is.readAllBytes(), StandardCharsets.UTF_8).trim();
					}
					break;

				case "photo":
					photoMediaType = p.getContentType();

					switch (photoMediaType.toLowerCase().trim()) {

						case "image/png":
						case "image/jpeg":
						case "image/jpg":
							// nothing to do
							break;

						default:
							LOGGER.error("Unsupported MIME media type %s for employee photo.", photoMediaType);

							throw new MimeTypeParseException(
									String.format("Unsupported MIME media type %s for employee photo.",
											photoMediaType));
					}

					try (InputStream is = p.getInputStream()) {
						photo = is.readAllBytes();
					}

					break;
			}

		}

		// creates a new employee from the request parameters
		return new Employee(badge, surname, age, salary, email, photo, photoMediaType);
	}

	/**
	 * Sends a confirmation e-mail upon successful creation of a new employee.
	 *
	 * @param e the just created employee.
	 *
	 * @throws MessagingException if something goes wrong while sending the email.
	 */
	private void sendCreationConfirmationEmail(Employee e) throws MessagingException {

		final StringBuilder sb = new StringBuilder();

		sb.append(String.format("<p>Dear %s,</p>%n", e.getSurname()));
		sb.append(String.format("<p>Your account has been successfully created as follows:</p>%n"));
		sb.append(String.format("<ul>%n"));
		sb.append(String.format("<li><b>badge</b>: %d</li>%n", e.getBadge()));
		sb.append(String.format("<li><b>surname</b>: %s</li>%n", e.getSurname()));
		sb.append(String.format("<li><b>age</b>: %d</li>%n", e.getAge()));
		sb.append(String.format("<li><b>salary</b>: %d</li>%n", e.getSalary()));

		if(e.hasPhoto()) {
			sb.append(String.format("<li><b>profile photo</b></li>%n"));
			sb.append(String.format("<ul>%n"));
			sb.append(String.format("<li><b>MIME media type</b>: %s</li>%n", e.getPhotoMediaType()));
			sb.append(String.format("<li><b>size</b>: %d byte(s)</li>%n", e.getPhotoSize()));
			sb.append(String.format("</ul>%n"));
		}

		sb.append(String.format("</ul>%n"));
		sb.append(String.format("<p>Best regards,<br>The EMPLOYEE Team</p>%n"));

		MailManager.sendMail(e.getEmail(), String.format("Employee %s successfully created.", e.getBadge()),
				sb.toString(), "text/html;charset=UTF-8");

	}

}
