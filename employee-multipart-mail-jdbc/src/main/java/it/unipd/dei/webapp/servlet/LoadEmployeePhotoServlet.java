/*
 * Copyright 2023 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.LoadEmployeePhotoDAO;
import it.unipd.dei.webapp.resource.Actions;
import it.unipd.dei.webapp.resource.Employee;
import it.unipd.dei.webapp.resource.LogContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * Loads the photo of an employee.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class LoadEmployeePhotoServlet extends AbstractDatabaseServlet {

	/**
	 * Loads the photo of an employee.
	 *
	 * @param req the HTTP request from the client.
	 * @param res the HTTP response from the server.
	 *
	 * @throws ServletException if any error occurs while executing the servlet.
	 * @throws IOException      if any error occurs in the client/server communication.
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		LogContext.setIPAddress(req.getRemoteAddr());
		LogContext.setAction(Actions.LOAD_EMPLOYEE_PHOTO);

		// request parameter
		int badge = -1;

		// model
		Employee e = null;

		try {

			// retrieves the request parameter
			badge = Integer.parseInt(req.getParameter("badge"));

			LogContext.setResource(req.getParameter("badge"));

			// creates a new object for accessing the database and loading the photo of an employees
			e = new LoadEmployeePhotoDAO(getConnection(), badge).access().getOutputParam();

			if (e.hasPhoto()) {
				res.setContentType(e.getPhotoMediaType());
				res.getOutputStream().write(e.getPhoto());
				res.getOutputStream().flush();

				LOGGER.info("Photo for employee %d successfully sent.", badge);
			} else {
				LOGGER.info("Employee %d has no profile photo and/or valide MIME media type specified.", badge);

				res.setStatus(HttpServletResponse.SC_NO_CONTENT);
			}

		} catch (Exception ex) {
			LOGGER.error("Unable to load the photo of the employee.", ex);

			res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} finally {
			LogContext.removeIPAddress();
			LogContext.removeAction();
			LogContext.removeUser();
		}

	}

}
