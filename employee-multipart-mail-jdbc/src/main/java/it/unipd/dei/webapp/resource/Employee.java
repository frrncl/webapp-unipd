/*
 * Copyright 2023 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.resource;

/**
 * Represents the data about an employee.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Employee {

	/**
	 * The badge number (identifier) of the employee
	 */
	private final int badge;

	/**
	 * The surname of the employee
	 */
	private final String surname;

	/**
	 * The age of the employee
	 */
	private final int age;

	/**
	 * The salary of the employee
	 */
	private final int salary;

	/**
	 * The email of the employee
	 */
	private final String email;

	/**
	 * The photo of the employee
	 */
	private final byte[] photo;

	/**
	 * The MIME media type of photo of the employee
	 */
	private final String photoMediaType;

	/**
	 * Creates a new employee
	 *
	 * @param badge          the badge number of the employee
	 * @param surname        the surname of the employee.
	 * @param age            the age of the employee.
	 * @param salary         the salary of the employee
	 * @param email          the email of the employee
	 * @param photo          the photo of the employee
	 * @param photoMediaType the MIME media type of photo of the employee
	 */
	public Employee(final int badge, final String surname, final int age, final int salary, final String email, final byte[] photo, final String photoMediaType) {
		this.badge = badge;
		this.surname = surname;
		this.age = age;
		this.salary = salary;
		this.email = email;
		this.photo = photo;
		this.photoMediaType = photoMediaType;
	}

	/**
	 * Returns the badge number of the employee.
	 *
	 * @return the badge number of the employee.
	 */
	public final int getBadge() {
		return badge;
	}

	/**
	 * Returns the surname of the employee.
	 *
	 * @return the surname of the employee.
	 */
	public final String getSurname() {
		return surname;
	}

	/**
	 * Returns the age of the employee.
	 *
	 * @return the age of the employee.
	 */
	public final int getAge() {
		return age;
	}

	/**
	 * Returns the salary of the employee.
	 *
	 * @return the salary of the employee.
	 */
	public final int getSalary() {
		return salary;
	}

	/**
	 * Returns the email of the employee.
	 *
	 * @return the email of the employee.
	 */
	public final String getEmail() {
		return email;
	}

	/**
	 * Returns the photo of the employee.
	 *
	 * @return the photo of the employee.
	 */
	public final byte[] getPhoto() {
		return photo;
	}

	/**
	 * Returns the MIME media type of photo of the employee.
	 *
	 * @return the MIME media type of photo of the employee.
	 */
	public final String getPhotoMediaType() {
		return photoMediaType;
	}

	/**
	 * Returns {@code true} if the {@code Employee} has a profile photo with a proper MIME media type specified;
	 * {@code false} otherwise.
	 *
	 * @return {@code true} if the {@code Employee} has a profile photo with a proper MIME media type specified; *
	 *        {@code false} otherwise.
	 */
	public final boolean hasPhoto() {
		return photo != null && photo.length > 0 && photoMediaType != null && !photoMediaType.isBlank();
	}

	/**
	 * Returns the size the of the profile photo, if any.
	 *
	 * @return the size the of the profile photo, if any; {@link Integer#MIN_VALUE} otherwise.
	 */
	public final int getPhotoSize() {
		return photo != null ? photo.length : Integer.MIN_VALUE;
	}


}
